# pylint: disable = no-member

from flask import Flask, request, json
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import and_
from flask_cors import CORS

app = Flask(__name__)
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql://dbme:dbme1234@mydbinstance.cn5qwaf8wmoa.us-east-2.rds.amazonaws.com/postgres"  # official database
db = SQLAlchemy(app)
CORS(app)

# ----------------------------Table definitions---------------------------------
class Politician(db.Model):
    __tablename__ = "politicians13"
    db_id = db.Column(db.Integer, primary_key=True)
    member_id = db.Column(db.Text)
    title = db.Column(db.Text)
    f_name = db.Column(db.Text)
    m_name = db.Column(db.Text)
    l_name = db.Column(db.Text)
    suffix = db.Column(db.Text)
    date_of_birth = db.Column(db.Text)
    gender = db.Column(db.Text)
    district = db.Column(db.Text)
    state = db.Column(db.Text)
    image_url = db.Column(db.Text)
    party = db.Column(db.Text)
    crp_id = db.Column(db.Text)
    # list all of them out?
    donoronename = db.Column(db.Text)
    donoroneamount = db.Column(db.Integer)
    donortwoname = db.Column(db.Text)
    donortwoamount = db.Column(db.Integer)
    donorthreename = db.Column(db.Text)
    donorthreeamount = db.Column(db.Integer)
    donorfourname = db.Column(db.Text)
    donorfouramount = db.Column(db.Integer)
    donorfivename = db.Column(db.Text)
    donorfiveamount = db.Column(db.Integer)
    twitter = db.Column(db.Text)
    office_years = db.Column(db.Integer)
    facebook = db.Column(db.Text)
    youtube = db.Column(db.Text)

    def __repr__(self):
        json_rep = {}
        json_rep["db_id"] = self.db_id
        json_rep["member_id"] = self.member_id
        json_rep["title"] = self.title
        json_rep["f_name"] = self.f_name
        json_rep["m_name"] = self.m_name
        json_rep["l_name"] = self.l_name
        json_rep["suffix"] = self.suffix
        json_rep["date_of_birth"] = self.date_of_birth
        json_rep["gender"] = self.gender
        json_rep["district"] = self.district
        json_rep["state"] = self.state
        json_rep["image_url"] = self.image_url
        json_rep["party"] = self.party
        json_rep["crp_id"] = self.crp_id
        json_rep["donorOneName"] = self.donoronename
        json_rep["donorOneAmount"] = self.donoroneamount
        json_rep["donorTwoName"] = self.donortwoname
        json_rep["donorTwoAmount"] = self.donortwoamount
        json_rep["donorThreeName"] = self.donorthreename
        json_rep["donorThreeAmount"] = self.donorthreeamount
        json_rep["donorFourName"] = self.donorfourname
        json_rep["donorFourAmount"] = self.donorfouramount
        json_rep["donorFiveName"] = self.donorfivename
        json_rep["donorFiveAmount"] = self.donorfiveamount
        json_rep["twitter"] = self.twitter
        json_rep["officeYears"] = self.office_years
        json_rep["facebook"] = self.facebook
        json_rep["youtube"] = self.youtube
        return json_rep


class District(db.Model):
    __tablename__ = "district6"
    db_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    image_url = db.Column(db.Text)
    major_city = db.Column(db.Text)
    population = db.Column(db.Integer)
    area = db.Column(db.Integer)
    median_income = db.Column(db.Integer)
    ethnicity = db.Column(db.Text)
    distribution = db.Column(db.Text)
    # put the list in one field?
    reps = db.Column(db.Text)
    rep_party = db.Column(db.Text)
    donors = db.Column(db.Text)

    def __repr__(self):
        json_rep = {}
        json_rep["db_id"] = self.db_id
        json_rep["name"] = self.name
        json_rep["imageurl"] = self.image_url
        json_rep["majorcity"] = self.major_city
        json_rep["population"] = self.population
        json_rep["area"] = self.area
        json_rep["median_income"] = self.median_income
        json_rep["ethnicity"] = self.ethnicity
        json_rep["distribution"] = self.distribution
        json_rep["reps"] = self.reps
        json_rep["rep_party"] = self.rep_party
        json_rep["donors"] = self.donors
        return json_rep


class Donor(db.Model):
    __tablename__ = "donor8"
    db_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    logo_url = db.Column(db.Text)
    rep_party = db.Column(db.Integer)
    dem_party = db.Column(db.Integer)
    indiv_donate = db.Column(db.Integer)
    pacs_donate = db.Column(db.Integer)
    state = db.Column(db.Text)
    city = db.Column(db.Text)
    country = db.Column(db.Text)
    twitter = db.Column(db.Text)
    facebook = db.Column(db.Text)
    website = db.Column(db.Text)
    phone_num = db.Column(db.Text)
    industry = db.Column(db.Text)
    district = db.Column(db.Text)
    rep = db.Column(db.Text)

    def __repr__(self):
        json_rep = {}
        json_rep["db_id"] = self.db_id
        json_rep["name"] = self.name
        json_rep["logourl"] = self.logo_url
        json_rep["repparty"] = self.rep_party
        json_rep["demparty"] = self.dem_party
        json_rep["indivdonate"] = self.indiv_donate
        json_rep["pacsdonate"] = self.pacs_donate
        json_rep["state"] = self.state
        json_rep["city"] = self.city
        json_rep["country"] = self.country
        json_rep["twitter"] = self.twitter
        json_rep["facebook"] = self.facebook
        json_rep["website"] = self.website
        json_rep["phone_num"] = self.phone_num
        json_rep["industry"] = self.industry
        json_rep["district"] = self.district
        json_rep["rep"] = self.rep
        return json_rep


# ------------------------------------------------------------------------------


def json_results(page, per_page, query):
    """
    Method that gets results from a query given page, per_page, and query. 

    :param {int} page:
        Page number we want to get data from. 
    :param {int} per_page:
        Number of results we are getting from each page. 
    :param {tuple} query:
        Object we are grabbing information from. 
    """

    results = []
    idx = page * per_page
    while idx < len(query) and idx < (page * per_page) + per_page:
        results.append(query[idx].__repr__())
        idx += 1
    results.append({"total_pgs": len(query) // per_page + 1})
    return results


def check_page_params(page, per_page, model):
    """
    Checks the parameters of page and per_page for a model. 

    :param {string} page:
        String representation of the page we are trying to grab information from. 
    :param {string} per_page:
        String representation of the number of results we want per page. 
    :param {int} model:
        Integer representation of which model we are operating on. 

    """

    if page == None and per_page == None:
        if model == 0:
            query = Politician.query.all()
        elif model == 1:
            query = Donor.query.all()
        else:  # model == 2
            query = District.query.all()
        pols = []
        for p in query:
            pols.append(p.__repr__())
        return json.jsonify(pols)
    elif page == None or per_page == None:
        return json.jsonify({"error": "missing page or per_page query"})
    for c in page:
        if c.isalpha():
            return json.jsonify({"error": "page is not a number"})
    for c in per_page:
        if c.isalpha():
            return json.jsonify({"error": "per_page is not a number"})
    if int(page) < 0 or int(per_page) < 0:
        return json.jsonify({"error": "invalid page or per_page query"})
    page = int(page)
    per_page = int(per_page)
    return (page, per_page)


@app.route("/politicians/")
def get_all_politicians():
    """
    Method used to get all the politicians in the database. 

    """

    page = request.args.get("page")
    per_page = request.args.get("per_page")
    ret = check_page_params(page, per_page, 0)
    if type(ret) != tuple:
        return ret
    page, per_page = ret
    query = (
        Politician.query.filter(Politician.db_id >= (page * per_page) + 1)
        .filter(Politician.db_id < (page * per_page) + per_page + 1)
        .all()
    )
    if len(query) == 0:
        return json.jsonify({"error": "invalid page number (too high)"})
    pols = []
    for p in query:
        pols.append(p.__repr__())
    pols.append({"total_pgs": Politician.query.count() // per_page + 1})
    return json.jsonify(pols)


# one sort and/or filter api link
@app.route("/politicians/sortfil")
def sort_filter_pols():
    """
    Method to sort and filter the politicians. 

    """

    sort_by = request.args.get("sort")  # what column to sort by (give correct col name)
    ascending = request.args.get("asc")  # order results in ascending/descending order
    fil = request.args.get("fil")
    page = request.args.get("page")
    per_page = request.args.get("per_page")

    ret = check_page_params(page, per_page, 0)
    if type(ret) != tuple:
        return ret
    page, per_page = ret

    if fil == None:
        if int(ascending):
            query = Politician.query.order_by(getattr(Politician, sort_by).asc()).all()
        else:
            query = Politician.query.order_by(getattr(Politician, sort_by).desc()).all()
    else:
        filters = [
            (fil_val.split("!")[0], fil_val.split("!")[1]) for fil_val in fil.split()
        ]
        filters = [
            getattr(Politician, fil_val[0]) == (fil_val[1]) for fil_val in filters
        ]
        if sort_by == None:
            query = Politician.query.filter(and_(*filters)).all()
        else:
            if int(ascending):
                query = (
                    Politician.query.order_by(getattr(Politician, sort_by).asc())
                    .filter(and_(*filters))
                    .all()
                )
            else:
                query = (
                    Politician.query.order_by(getattr(Politician, sort_by).desc())
                    .filter(and_(*filters))
                    .all()
                )

    return json.jsonify(json_results(page, per_page, query))


@app.route("/politician")
def get_pol_name():
    """
    Method used to get a specific politician by their first and last name. 

    """

    first = request.args.get("first")
    last = request.args.get("last")
    query = Politician.query.filter_by(f_name=first, l_name=last).first_or_404()
    return json.jsonify(query.__repr__())


@app.route("/donors/")
def get_all_donors():
    """
    Method used to get all the donors in the database. ;

    """

    page = request.args.get("page")
    per_page = request.args.get("per_page")
    ret = check_page_params(page, per_page, 1)
    if type(ret) != tuple:
        return ret
    page, per_page = ret
    query = (
        Donor.query.filter(Donor.db_id >= (page * per_page) + 1)
        .filter(Donor.db_id < (page * per_page) + per_page + 1)
        .all()
    )
    if len(query) == 0:
        return json.jsonify({"error": "invalid page number (too high)"})
    pols = []
    for p in query:
        pols.append(p.__repr__())
    pols.append({"total_pgs": Donor.query.count() // per_page + 1})
    return json.jsonify(pols)


# one sort and/or filter api link
@app.route("/donors/sortfil")
def sort_filter_dons():
    """ 
    Method used to sort and filter the donors. 

    """

    sort_by = request.args.get("sort")  # what column to sort by (give correct col name)
    ascending = request.args.get("asc")  # order results in ascending/descending order
    fil = request.args.get("fil")
    page = request.args.get("page")
    per_page = request.args.get("per_page")

    ret = check_page_params(page, per_page, 1)
    if type(ret) != tuple:
        return ret
    page, per_page = ret

    if not fil:
        if int(ascending):
            query = Donor.query.order_by(getattr(Donor, sort_by).asc()).all()
        else:
            query = Donor.query.order_by(getattr(Donor, sort_by).desc()).all()
    else:
        filters = [
            (fil_val.split("!")[0], fil_val.split("!")[1]) for fil_val in fil.split()
        ]
        # filters = [getattr(Donor, fil_val[0]) == (fil_val[1]) for fil_val in filters]
        for i in range(len(filters)):
            if filters[i][1].isdigit():
                filters[i] = getattr(Donor, filters[i][0]) > (filters[i][1])
            else:
                filters[i] = getattr(Donor, filters[i][0]) == (filters[i][1])
        if sort_by == None:
            query = Donor.query.filter(and_(*filters)).all()
        else:
            if int(ascending):
                query = (
                    Donor.query.order_by(getattr(Donor, sort_by).asc())
                    .filter(and_(*filters))
                    .all()
                )
            else:
                query = (
                    Donor.query.order_by(getattr(Donor, sort_by).desc())
                    .filter(and_(*filters))
                    .all()
                )

    return json.jsonify(json_results(page, per_page, query))


@app.route("/donor/<name>")
def get_donor_name(name):
    """
    Method used to filter donors by their name. 

    :param {string} name: 
        Possible name of the donor. 
    """

    query = Donor.query.filter_by(name=name).first_or_404()
    return json.jsonify(query.__repr__())


@app.route("/districts/")
def get_all_districts():
    """
    Method used to get all the districts in the database. 

    """

    page = request.args.get("page")
    per_page = request.args.get("per_page")
    ret = check_page_params(page, per_page, 2)
    if type(ret) != tuple:
        return ret
    page, per_page = ret
    query = (
        District.query.filter(District.db_id >= (page * per_page) + 1)
        .filter(District.db_id < (page * per_page) + per_page + 1)
        .all()
    )
    if len(query) == 0:
        return json.jsonify({"error": "invalid page number (too high)"})
    pols = []
    for p in query:
        pols.append(p.__repr__())
    pols.append({"total_pgs": District.query.count() // per_page + 1})
    return json.jsonify(pols)


# one sort and/or filter api link
@app.route("/districts/sortfil")
def sort_filter_dis():
    """
    Method used to sort and filter the districts. 

    """

    sort_by = request.args.get("sort")  # what column to sort by (give correct col name)
    ascending = request.args.get("asc")  # order results in ascending/descending order
    fil = request.args.get("fil")
    page = request.args.get("page")
    per_page = request.args.get("per_page")

    ret = check_page_params(page, per_page, 2)
    if type(ret) != tuple:
        return ret
    page, per_page = ret

    if not fil:
        if int(ascending):
            query = District.query.order_by(getattr(District, sort_by).asc()).all()
        else:
            query = District.query.order_by(getattr(District, sort_by).desc()).all()
    else:
        filters = [
            (fil_val.split("!")[0], fil_val.split("!")[1]) for fil_val in fil.split()
        ]
        # filters = [getattr(District, fil_val[0]) == (fil_val[1]) for fil_val in filters]

        for i in range(len(filters)):
            if filters[i][1].isdigit():
                filters[i] = getattr(District, filters[i][0]) > (filters[i][1])
            else:
                filters[i] = getattr(District, filters[i][0]) == (filters[i][1])
        if sort_by == None:
            query = District.query.filter(and_(*filters)).all()
        else:
            if int(ascending):
                query = (
                    District.query.order_by(getattr(District, sort_by).asc())
                    .filter(and_(*filters))
                    .all()
                )
            else:
                query = (
                    District.query.order_by(getattr(District, sort_by).desc())
                    .filter(and_(*filters))
                    .all()
                )

    return json.jsonify(json_results(page, per_page, query))


@app.route("/district/<name>")
def get_district(name):
    """ 
    Method used to filter districts by their name. 

    :param {string} name: 
        Name of a particular district. 

    """

    query = District.query.filter_by(name=name).first_or_404()
    return json.jsonify(query.__repr__())


if __name__ == "__main__":
    app.run("0.0.0.0", "80")
