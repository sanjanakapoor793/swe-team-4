from app import db

class Politician(db.Model):
    __tablename__ = "politicians13"
    db_id = db.Column(db.Integer, primary_key=True)
    member_id = db.Column(db.Text)
    title = db.Column(db.Text)
    f_name = db.Column(db.Text)
    m_name = db.Column(db.Text)
    l_name = db.Column(db.Text)
    suffix = db.Column(db.Text)
    date_of_birth = db.Column(db.Text)
    gender = db.Column(db.Text)
    district = db.Column(db.Text)
    state = db.Column(db.Text)
    image_url = db.Column(db.Text)
    party = db.Column(db.Text)
    crp_id = db.Column(db.Text)
    # list all of them out?
    donoronename = db.Column(db.Text)
    donoroneamount = db.Column(db.Integer)
    donortwoname = db.Column(db.Text)
    donortwoamount = db.Column(db.Integer)
    donorthreename = db.Column(db.Text)
    donorthreeamount = db.Column(db.Integer)
    donorfourname = db.Column(db.Text)
    donorfouramount = db.Column(db.Integer)
    donorfivename = db.Column(db.Text)
    donorfiveamount = db.Column(db.Integer)
    twitter = db.Column(db.Text)
    office_years = db.Column(db.Integer)
    facebook = db.Column(db.Text)
    youtube = db.Column(db.Text)

    def __repr__(self):
        json_rep = {}
        json_rep["db_id"] = self.db_id
        json_rep["member_id"] = self.member_id
        json_rep["title"] = self.title
        json_rep["f_name"] = self.f_name
        json_rep["m_name"] = self.m_name
        json_rep["l_name"] = self.l_name
        json_rep["suffix"] = self.suffix
        json_rep["date_of_birth"] = self.date_of_birth
        json_rep["gender"] = self.gender
        json_rep["district"] = self.district
        json_rep["state"] = self.state
        json_rep["image_url"] = self.image_url
        json_rep["party"] = self.party
        json_rep["crp_id"] = self.crp_id
        json_rep["donorOneName"] = self.donoronename
        json_rep["donorOneAmount"] = self.donoroneamount
        json_rep["donorTwoName"] = self.donortwoname
        json_rep["donorTwoAmount"] = self.donortwoamount
        json_rep["donorThreeName"] = self.donorthreename
        json_rep["donorThreeAmount"] = self.donorthreeamount
        json_rep["donorFourName"] = self.donorfourname
        json_rep["donorFourAmount"] = self.donorfouramount
        json_rep["donorFiveName"] = self.donorfivename
        json_rep["donorFiveAmount"] = self.donorfiveamount
        json_rep["twitter"] = self.twitter
        json_rep["officeYears"] = self.office_years
        json_rep["facebook"] = self.facebook
        json_rep["youtube"] = self.youtube
        return json_rep


class District(db.Model):
    __tablename__ = "district6"
    db_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    image_url = db.Column(db.Text)
    major_city = db.Column(db.Text)
    population = db.Column(db.Integer)
    area = db.Column(db.Integer)
    median_income = db.Column(db.Integer)
    ethnicity = db.Column(db.Text)
    distribution = db.Column(db.Text)
    # put the list in one field?
    reps = db.Column(db.Text)
    rep_party = db.Column(db.Text)
    donors = db.Column(db.Text)

    def __repr__(self):
        json_rep = {}
        json_rep["db_id"] = self.db_id
        json_rep["name"] = self.name
        json_rep["imageurl"] = self.image_url
        json_rep["majorcity"] = self.major_city
        json_rep["population"] = self.population
        json_rep["area"] = self.area
        json_rep["median_income"] = self.median_income
        json_rep["ethnicity"] = self.ethnicity
        json_rep["distribution"] = self.distribution
        json_rep["reps"] = self.reps
        json_rep["rep_party"] = self.rep_party
        json_rep["donors"] = self.donors
        return json_rep


class Donor(db.Model):
    __tablename__ = "donor8"
    db_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    logo_url = db.Column(db.Text)
    rep_party = db.Column(db.Integer)
    dem_party = db.Column(db.Integer)
    indiv_donate = db.Column(db.Integer)
    pacs_donate = db.Column(db.Integer)
    state = db.Column(db.Text)
    city = db.Column(db.Text)
    country = db.Column(db.Text)
    twitter = db.Column(db.Text)
    facebook = db.Column(db.Text)
    website = db.Column(db.Text)
    phone_num = db.Column(db.Text)
    industry = db.Column(db.Text)
    district = db.Column(db.Text)
    rep = db.Column(db.Text)

    def __repr__(self):
        json_rep = {}
        json_rep["db_id"] = self.db_id
        json_rep["name"] = self.name
        json_rep["logourl"] = self.logo_url
        json_rep["repparty"] = self.rep_party
        json_rep["demparty"] = self.dem_party
        json_rep["indivdonate"] = self.indiv_donate
        json_rep["pacsdonate"] = self.pacs_donate
        json_rep["state"] = self.state
        json_rep["city"] = self.city
        json_rep["country"] = self.country
        json_rep["twitter"] = self.twitter
        json_rep["facebook"] = self.facebook
        json_rep["website"] = self.website
        json_rep["phone_num"] = self.phone_num
        json_rep["industry"] = self.industry
        json_rep["district"] = self.district
        json_rep["rep"] = self.rep
        return json_rep