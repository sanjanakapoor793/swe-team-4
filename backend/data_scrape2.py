import json
from requests.auth import HTTPBasicAuth
import requests
import xml.etree.ElementTree as ET

import psycopg2
import sys, os
import numpy as np
import pandas as pd

# import example_psql as creds
import pandas.io.sql as psql


headers = {"Content-Type": "application/json"}


def all_states():
    hostname = "mydbinstance.cn5qwaf8wmoa.us-east-2.rds.amazonaws.com"
    # hostname = 'localhost'
    username = "dbme"
    password = "dbme1234"
    dataabase = "postgres"

    myConnection = psycopg2.connect(
        database=dataabase, user=username, password=password, host=hostname, port="5432"
    )

    cur = myConnection.cursor()
    cur.execute("SELECT * from politicians_tb")
    print(cur.fetchone())

    states = [
        "AL",
        "AK",
        "AZ",
        "AR",
        "CA",
        "CO",
        "CT",
        "DC",
        "DE",
        "FL",
        "GA",
        "HI",
        "ID",
        "IL",
        "IN",
        "IA",
        "KS",
        "KY",
        "LA",
        "ME",
        "MD",
        "MA",
        "MI",
        "MN",
        "MS",
        "MO",
        "MT",
        "NE",
        "NV",
        "NH",
        "NJ",
        "NM",
        "NY",
        "NC",
        "ND",
        "OH",
        "OK",
        "OR",
        "PA",
        "RI",
        "SC",
        "SD",
        "TN",
        "TX",
        "UT",
        "VT",
        "VA",
        "WA",
        "WV",
        "WI",
        "WY",
    ]
    for state in states:
        final_api_url = (
            "http://www.opensecrets.org/api/?method=getLegislators&id="
            + state
            + "&apikey=8b8ffb980f1338a687b72f9f25f89d47"
        )

        response = requests.get(final_api_url, headers=headers)
        if response.status_code == 200:
            result = ET.fromstring(response.content)
            for child in result:

                # child.attrib is the dictionary with all of the information
                names = child.attrib["firstlast"]
                cid = child.attrib["cid"]
                donations = get_donors_pol(cid)
                # print(donations is None)

                if donations is None:
                    query = "INSERT INTO politicians5 (name, state, party, twitter) VALUES (%s, %s, %s, %s)"
                    data = (
                        child.attrib["firstlast"],
                        state,
                        child.attrib["party"],
                        child.attrib["twitter_id"],
                    )

                elif len(donations) == 1:
                    query = "INSERT INTO politicians5 (name, state, party, donoronename, donoroneamount, twitter) VALUES (%s, %s, %s, %s, %s, %s)"

                    data = (
                        child.attrib["firstlast"],
                        state,
                        child.attrib["party"],
                        donations[0]["org_name"],
                        donations[0]["total"],
                        child.attrib["twitter_id"],
                    )

                elif len(donations) == 2:
                    query = "INSERT INTO politicians5 (name, state, party, donoronename, donoroneamount, donortwoname, donortwoamount, \
						twitter) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"

                    data = (
                        child.attrib["firstlast"],
                        state,
                        child.attrib["party"],
                        donations[0]["org_name"],
                        donations[0]["total"],
                        donations[1]["org_name"],
                        donations[1]["total"],
                        child.attrib["twitter_id"],
                    )

                elif len(donations) == 3:
                    query = "INSERT INTO politicians5 (name, state, party, donoronename, donoroneamount, donortwoname, donortwoamount, donorthreename, donorthreeamount, twitter) \
						VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

                    data = (
                        child.attrib["firstlast"],
                        state,
                        child.attrib["party"],
                        donations[0]["org_name"],
                        donations[0]["total"],
                        donations[1]["org_name"],
                        donations[1]["total"],
                        donations[2]["org_name"],
                        donations[2]["total"],
                        child.attrib["twitter_id"],
                    )

                else:
                    query = "INSERT INTO politicians5 (name, state, party, donoronename, donoroneamount, donortwoname, donortwoamount, donorthreename, donorthreeamount, \
						donorfourname, donorfouramount, twitter) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

                    data = (
                        child.attrib["firstlast"],
                        state,
                        child.attrib["party"],
                        donations[0]["org_name"],
                        donations[0]["total"],
                        donations[1]["org_name"],
                        donations[1]["total"],
                        donations[2]["org_name"],
                        donations[2]["total"],
                        donations[3]["org_name"],
                        donations[3]["total"],
                        child.attrib["twitter_id"],
                    )

                cur.execute(query, data)
                myConnection.commit()

                # use the cid to get the information out of the next api for donations
                # use the name to get the picture from wikipedia
        else:
            raise Exception()
            print("status code: " + str(response.status_code))
            print("did not work")


def get_donors_pol(cid):
    donor_api_url = (
        "https://www.opensecrets.org/api/?method=candContrib&cid="
        + cid
        + "&cycle=2018&apikey=93ddc2b746ec19ced875b169e81ccc29"
    )
    response = requests.get(donor_api_url, headers=headers)
    if response.status_code == 200:
        result = ET.fromstring(response.content)
        for child in result:
            result = []
            count = 0
            for c in child:
                if count < 5:
                    result.append(c.attrib)
                    count += 1
            print(result)
            return result
    else:
        print("did not work")
        print(str(cid))


def other_api():
    hostname = "mydbinstance.cn5qwaf8wmoa.us-east-2.rds.amazonaws.com"
    username = "dbme"
    password = "dbme1234"
    dataabase = "postgres"

    myConnection = psycopg2.connect(
        database=dataabase, user=username, password=password, host=hostname, port="5432"
    )

    cur = myConnection.cursor()

    headers = {
        "Content-Type": "application/json",
        "X-API-Key": "pjVNsrGJ6b4uFfTXd08ULNe9sT88AxO8MSwM2fGR",
    }
    response = requests.get(
        "https://api.propublica.org/congress/v1/115/senate/members.json",
        headers=headers,
    )
    if response.status_code == 200:
        x = response.content.decode("utf-8")
        y = json.loads(x)
        list_pol = y["results"][0]["members"]

        for pol in list_pol:
            member_id = pol["id"]
            title = pol["title"]
            f_name = pol["first_name"]
            m_name = pol["middle_name"]
            l_name = pol["last_name"]
            suffix = pol["suffix"]
            dob = pol["date_of_birth"]
            gender = pol["gender"]
            party = pol["party"]
            twitter = pol["twitter_account"]
            crp_id = pol["crp_id"]
            state = pol["state"]
            image_url = (
                "https://theunitedstates.io/images/congress/original/"
                + member_id
                + ".jpg"
            )

            if crp_id is None:
                donations = None
            else:
                donations = get_donors_pol(crp_id)
            print(donations is None)

            if donations is None:
                query = "INSERT INTO politicians12 (member_id, title, f_name, m_name, l_name, suffix, date_of_birth, gender, party, twitter, image_url, \
					crp_id, state) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                data = (
                    member_id,
                    title,
                    f_name,
                    m_name,
                    l_name,
                    suffix,
                    dob,
                    gender,
                    party,
                    twitter,
                    image_url,
                    crp_id,
                    state,
                )

            elif len(donations) == 1:
                query = "INSERT INTO politicians12 (member_id, title, f_name, m_name, l_name, suffix, date_of_birth, gender, party, twitter, image_url, \
					crp_id, state, donoronename, donoroneamount) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

                data = (
                    member_id,
                    title,
                    f_name,
                    m_name,
                    l_name,
                    suffix,
                    dob,
                    gender,
                    party,
                    twitter,
                    image_url,
                    crp_id,
                    state,
                    donations[0]["org_name"],
                    donations[0]["total"],
                )

            elif len(donations) == 2:
                query = "INSERT INTO politicians12 (member_id, title, f_name, m_name, l_name, suffix, date_of_birth, gender, party, twitter, image_url, \
					crp_id, state, donoronename, donoroneamount, donortwoname, donortwoamount) VALUES %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

                data = (
                    member_id,
                    title,
                    f_name,
                    m_name,
                    l_name,
                    suffix,
                    dob,
                    gender,
                    party,
                    twitter,
                    image_url,
                    crp_id,
                    state,
                    donations[0]["org_name"],
                    donations[0]["total"],
                    donations[1]["org_name"],
                    donations[1]["total"],
                )

            elif len(donations) == 3:
                query = "INSERT INTO politicians12 (member_id, title, f_name, m_name, l_name, suffix, date_of_birth, gender, party, twitter, image_url, \
					crp_id, state, donoronename, donoroneamount, donortwoname, donortwoamount, donorthreename, donorthreeamount) \
						VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

                data = (
                    member_id,
                    title,
                    f_name,
                    m_name,
                    l_name,
                    suffix,
                    dob,
                    gender,
                    party,
                    twitter,
                    image_url,
                    crp_id,
                    state,
                    donations[0]["org_name"],
                    donations[0]["total"],
                    donations[1]["org_name"],
                    donations[1]["total"],
                    donations[2]["org_name"],
                    donations[2]["total"],
                )

            else:
                query = "INSERT INTO politicians12 (member_id, title, f_name, m_name, l_name, suffix, date_of_birth, gender, party, twitter, image_url, \
					crp_id, state, donoronename, donoroneamount, donortwoname, donortwoamount, donorthreename, donorthreeamount, \
						donorfourname, donorfouramount) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

                data = (
                    member_id,
                    title,
                    f_name,
                    m_name,
                    l_name,
                    suffix,
                    dob,
                    gender,
                    party,
                    twitter,
                    image_url,
                    crp_id,
                    state,
                    donations[0]["org_name"],
                    donations[0]["total"],
                    donations[1]["org_name"],
                    donations[1]["total"],
                    donations[2]["org_name"],
                    donations[2]["total"],
                    donations[3]["org_name"],
                    donations[3]["total"],
                )

            cur.execute(query, data)
            myConnection.commit()

    else:
        print("did not work")


def legis_val():
    hostname = "mydbinstance.cn5qwaf8wmoa.us-east-2.rds.amazonaws.com"
    username = "dbme"
    password = "dbme1234"
    dataabase = "postgres"

    myConnection = psycopg2.connect(
        database=dataabase, user=username, password=password, host=hostname, port="5432"
    )

    cur = myConnection.cursor()

    headers = {
        "Content-Type": "application/json",
        "X-API-Key": "pjVNsrGJ6b4uFfTXd08ULNe9sT88AxO8MSwM2fGR",
    }

    response = requests.get(
        "https://api.propublica.org/congress/v1/115/house/members.json", headers=headers
    )
    if response.status_code == 200:
        x = response.content.decode("utf-8")
        print(type(x))
        y = json.loads(x)
        list_pol = y["results"][0]["members"]
        print(len(list_pol))
        print(list_pol[0])

        for pol in list_pol:
            member_id = pol["id"]
            title = pol["title"]
            f_name = pol["first_name"]
            m_name = pol["middle_name"]
            l_name = pol["last_name"]
            suffix = pol["suffix"]
            dob = pol["date_of_birth"]
            gender = pol["gender"]
            party = pol["party"]
            twitter = pol["twitter_account"]
            district = pol["district"]
            crp_id = pol["crp_id"]
            state = pol["state"]
            image_url = (
                "https://theunitedstates.io/images/congress/original/"
                + member_id
                + ".jpg"
            )

            if crp_id is None:
                donations = None
            else:
                donations = get_donors_pol(crp_id)
            print(donations is None)

            if donations is None:
                query = "INSERT INTO politicians12 (member_id, title, f_name, m_name, l_name, suffix, date_of_birth, gender, party, twitter, image_url, \
					crp_id, state, district) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                data = (
                    member_id,
                    title,
                    f_name,
                    m_name,
                    l_name,
                    suffix,
                    dob,
                    gender,
                    party,
                    twitter,
                    image_url,
                    crp_id,
                    state,
                    district,
                )

            elif len(donations) == 1:
                query = "INSERT INTO politicians12 (member_id, title, f_name, m_name, l_name, suffix, date_of_birth, gender, party, twitter, image_url, \
					crp_id, state, district, donoronename, donoroneamount) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

                data = (
                    member_id,
                    title,
                    f_name,
                    m_name,
                    l_name,
                    suffix,
                    dob,
                    gender,
                    party,
                    twitter,
                    image_url,
                    crp_id,
                    state,
                    district,
                    donations[0]["org_name"],
                    donations[0]["total"],
                )

            elif len(donations) == 2:
                query = "INSERT INTO politicians12 (member_id, title, f_name, m_name, l_name, suffix, date_of_birth, gender, party, twitter, image_url, \
					crp_id, state, district, donoronename, donoroneamount, donortwoname, donortwoamount) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

                data = (
                    member_id,
                    title,
                    f_name,
                    m_name,
                    l_name,
                    suffix,
                    dob,
                    gender,
                    party,
                    twitter,
                    image_url,
                    crp_id,
                    state,
                    district,
                    donations[0]["org_name"],
                    donations[0]["total"],
                    donations[1]["org_name"],
                    donations[1]["total"],
                )

            elif len(donations) == 3:
                query = "INSERT INTO politicians12 (member_id, title, f_name, m_name, l_name, suffix, date_of_birth, gender, party, twitter, image_url, \
					crp_id, state, district, donoronename, donoroneamount, donortwoname, donortwoamount, donorthreename, donorthreeamount) \
						VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

                data = (
                    member_id,
                    title,
                    f_name,
                    m_name,
                    l_name,
                    suffix,
                    dob,
                    gender,
                    party,
                    twitter,
                    image_url,
                    crp_id,
                    state,
                    district,
                    donations[0]["org_name"],
                    donations[0]["total"],
                    donations[1]["org_name"],
                    donations[1]["total"],
                    donations[2]["org_name"],
                    donations[2]["total"],
                )

            else:
                query = "INSERT INTO politicians12 (member_id, title, f_name, m_name, l_name, suffix, date_of_birth, gender, party, twitter, image_url, \
					crp_id, state, district, donoronename, donoroneamount, donortwoname, donortwoamount, donorthreename, donorthreeamount, \
						donorfourname, donorfouramount) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

                data = (
                    member_id,
                    title,
                    f_name,
                    m_name,
                    l_name,
                    suffix,
                    dob,
                    gender,
                    party,
                    twitter,
                    image_url,
                    crp_id,
                    state,
                    district,
                    donations[0]["org_name"],
                    donations[0]["total"],
                    donations[1]["org_name"],
                    donations[1]["total"],
                    donations[2]["org_name"],
                    donations[2]["total"],
                    donations[3]["org_name"],
                    donations[3]["total"],
                )

            cur.execute(query, data)
            myConnection.commit()

    else:
        print("did not work")
