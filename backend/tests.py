import unittest
import requests
import json


url = "https://api.politicianstakingmoney.me/"


class TestMemberMethods(unittest.TestCase):
    def testConnect(self):
        r = requests.get(url + "politicians/")
        self.assertEqual(r.status_code, 200)

    def testNotFound(self):
        r = requests.get(url + "politician?first=bigshot&last=mac&suffix=Dr.")
        self.assertEqual(r.status_code, 404)

    def testPolName(self):
        r = requests.get(url + "politician?first=Ralph&last=Abraham")
        d = json.loads(r.text)
        n = d["f_name"] + " " + d["l_name"]
        self.assertEqual(n, "Ralph Abraham")

    def testDonorName(self):
        r = requests.get(url + "donor/Facebook Inc")
        d = json.loads(r.text)
        n = d["name"]
        self.assertEqual(n, "Facebook Inc")

    def testDistrictName(self):
        r = requests.get(url + "district/Alabama-1")
        d = json.loads(r.text)
        n = d["name"]
        self.assertEqual(n, "Alabama-1")

    def testPols(self):
        r = requests.get(url + "politicians?page=10&per_page=10")
        d = json.loads(r.text)
        self.assertEqual(len(d), 11)

    def testDonors(self):
        r = requests.get(url + "donors?page=1&per_page=9")
        d = json.loads(r.text)
        self.assertEqual(len(d), 10)

    def testDistricts(self):
        r = requests.get(url + "districts?page=0&per_page=15")
        d = json.loads(r.text)
        self.assertEqual(len(d), 16)

    def testPolTypes(self):
        r = requests.get(url + "politician?first=Ralph&last=Abraham")
        d = json.loads(r.text)
        n = d["f_name"]
        o = d["party"]
        self.assertIsInstance(n, str)
        self.assertIsInstance(o, str)

    def testDonorTypes(self):
        r = requests.get(url + "donor/Facebook Inc")
        d = json.loads(r.text)
        n = d["name"]
        o = d["demparty"]
        self.assertIsInstance(n, str)
        self.assertIsInstance(o, int)

    def testDistrictTypes(self):
        r = requests.get(url + "district/Alabama-1")
        d = json.loads(r.text)
        m = d["majorcity"]
        o = d["population"]
        self.assertIsInstance(m, str)
        self.assertIsInstance(o, int)

    def testPolFilter(self):
        r = requests.get(url + "politicians/sortfil?fil=f_name!Ralph&page=0&per_page=5")
        d = json.loads(r.text)
        n = d[0]["f_name"]
        self.assertEqual(n, "Ralph")

    def testDonorFilter(self):
        r = requests.get(url + "donors/sortfil?fil=name!Kidsave&page=0&per_page=5")
        d = json.loads(r.text)
        n = d[0]["name"]
        self.assertEqual(n, "Kidsave")

    def testDistrictFilter(self):
        r = requests.get(url + "districts/sortfil?fil=name!Alabama-3&page=0&per_page=5")
        d = json.loads(r.text)
        n = d[0]["name"]
        self.assertEqual(n, "Alabama-3")

    def testPolSort(self):
        r = requests.get(
            url + "politicians/sortfil?sort=state&asc=1&page=0&per_page=10"
        )
        d = json.loads(r.text)
        n = d[0]["state"]
        self.assertEqual(n, "AL")

    def testDonorSort(self):
        r = requests.get(url + "donors/sortfil?sort=industry&asc=1&page=0&per_page=10")
        d = json.loads(r.text)
        n = d[0]["industry"]
        self.assertEqual(n, "Aerospace & Defense")

    def testDistrictSort(self):
        r = requests.get(
            url + "districts/sortfil?sort=median_income&asc=1&page=0&per_page=10"
        )
        d = json.loads(r.text)
        n = d[0]["median_income"]
        self.assertEqual(n, 35988)

    def testPolSortDesc(self):
        r = requests.get(
            url + "politicians/sortfil?sort=f_name&asc=0&page=0&per_page=10"
        )
        d = json.loads(r.text)
        n = d[0]["f_name"]
        self.assertEqual(n, "Yvette")

    def testDonorSortDesc(self):
        r = requests.get(url + "donors/sortfil?sort=name&asc=0&page=0&per_page=10")
        d = json.loads(r.text)
        n = d[0]["name"]
        self.assertEqual(n, "Webco General Partnership")

    def testDistrictSortDesc(self):
        r = requests.get(url + "districts/sortfil?sort=name&asc=0&page=0&per_page=10")
        d = json.loads(r.text)
        n = d[0]["name"]
        self.assertEqual(n, "Colorado-5")


if __name__ == "__main__":
    unittest.main()
