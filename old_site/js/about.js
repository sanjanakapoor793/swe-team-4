const PROJECTID = 10903935;
const ISSUESURL = `https://gitlab.com/api/v4/projects/${PROJECTID}/issues`;
const COMMITSURL = `https://gitlab.com/api/v4/projects/${PROJECTID}/repository/commits?per_page=999`;

function displayCommits(res){
    let data = res;

    let total = 0;
    let userCommits =
    {
        aj: 0,
        sr: 0,
        ln: 0,
        sk: 0,
        cb: 0
    };

    for(let i = 0; i < data.length; i++){
        switch(data[i].author_name){
            case 'aarjo':
            case 'Aaron Johnson':
                userCommits.aj++;
                break;
            case 'bec7992':
            case 'Clinton Bell':
                userCommits.cb++;
                break;
            case 'lbn99':
            case 'LB Nguyen':
                userCommits.ln++;
                break;
            case 'sanjanakapoor793':
            case 'Sanjana Kapoor':
                userCommits.sk++;
                break;
            case 'slycane9':
            case 'Serdjan Rolovic':
                userCommits.sr++;
                break;
        }
        total++;
    }

    document.getElementById("ajcommits").innerHTML = userCommits.aj;
    document.getElementById("srcommits").innerHTML = userCommits.sr;
    document.getElementById("lncommits").innerHTML = userCommits.ln;
    document.getElementById("skcommits").innerHTML = userCommits.sk;
    document.getElementById("cbcommits").innerHTML = userCommits.cb;
    document.getElementById("tcommits").innerHTML = total;

}

function displayIssues(res){
    let data = res;

    let total = 0;
    let userIssues = 
    {
        aj: 0,
        sr: 0,
        ln: 0,
        sk: 0,
        cb: 0
    };

    for(let i = 0; i < data.length; i++){
        switch(data[i].author.username){
            case 'aarjo':
                userIssues.aj++;
                break;
            case 'bec7992':
                userIssues.cb++;
                break;
            case 'lbn99':
                userIssues.ln++;
                break;
            case 'slycane9':
                userIssues.sr++;
                break;
            case 'sanjanakapoor793':
                userIssues.sk++;
                break;
        }
        total++;
    }

    document.getElementById("ajissues").innerHTML = userIssues.aj;
    document.getElementById("srissues").innerHTML = userIssues.sr;
    document.getElementById("lnissues").innerHTML = userIssues.ln;
    document.getElementById("skissues").innerHTML = userIssues.sk;
    document.getElementById("cbissues").innerHTML = userIssues.cb;
    document.getElementById("tissues").innerHTML = total;
}

fetch(COMMITSURL).then((res) => res.json().then(displayCommits));
fetch(ISSUESURL).then((res) => res.json().then(displayIssues));
