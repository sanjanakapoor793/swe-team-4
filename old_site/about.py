"""
gets info from gitlab repo and interprets info and displays on html page
"""

import requests

url = "https://gitlab.com/api/v4/projects/10903935/repository/commits"
headers = {"PRIVATE-TOKEN": "sSmVzgK1zVLxf_YDkz9F"}
resp = requests.get(url, headers=headers)

# ret = resp.json()
# print(ret)
# print()
# print(ret[0].keys())

commit_count = {}

for commit in resp.json():
    # print("-------------------------------------------------------------------")
    # print("Commit ID: " + str(commit["id"]))
    # print("Message: " + str(commit["message"]))
    # print("Committer: " + str(commit["committer_name"]))
    # print("-------------------------------------------------------------------")

    author = commit["committer_name"]
    if author in commit_count:
        commit_count[author] += 1
    else:
        commit_count[author] = 1

for author in commit_count:
    print(str(author) + " commits: " + str(commit_count[author]))
