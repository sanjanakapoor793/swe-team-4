# SWE Team 4 - Politicians Taking Money

Name: Clinton Bell  
EID: ceb4343  
GitLab ID: bec7992  
Phase 1 Estimation Time: 15 hr/wk  
Phase 1 Actual Time: 13 hr/wk  
Phase 2 Estimation Time: 20 hr/wk  
Phase 2 Actual Time: 18 hr/wk  
Phase 3 Estimation Time: 18 hr/wk  
Phase 3 Actual Time: 15 hr/wk    
Phase 4 Estimation Time:    
Phase 4 Actual Time:    
  
Name: Aaron Johnson  
EID: amj3499  
GitLab ID: aarjo  
Phase 1 Estimation Time: 15 hr/wk  
Phase 1 Actual Time: 9 hr/wk  
Phase 2 Estimation Time: 20 hr/wk  
Phase 2 Actual Time: 16 hr/wk  
Phase 3 Estimation Time: 17 hr/wk  
Phase 3 Actual Time: 16 hr/wk    
Phase 4 Estimation Time: 15 hr/wk   
Phase 4 Actual Time: 17 hr/wk   
  
Name: Sanjana Kapoor  
EID: sk45257  
GitLab ID: sanjanakapoor793  
Phase 1 Estimation Time: 15 hr/wk  
Phase 1 Actual Time: 9.5 hr/wk  
Phase 2 Estimation Time: 20 hr/wk  
Phase 2 Actual Time: 16.5 hr/wk   
Phase 3 Estimation Time: 19 hr/wk  
Phase 3 Actual Time: 15 hr/wk    
Phase 4 Estimation Time: 16 hr/wk   
Phase 4 Actual Time: 15 hr/wk   

Name: Lyndon Nguyen  
EID: ln6528  
GitLab ID: lbn99  
Phase 1 Estimation Time: 15 hr/wk  
Phase 1 Actual Time: 9 hr/wk  
Phase 2 Estimation Time: 20 hr/wk  
Phase 2 Actual Time: 17 hr/wk   
Phase 3 Estimation Time: 18 hr/wk  
Phase 3 Actual Time: 15 hr/wk    
Phase 4 Estimation Time: 17 hr/wk   
Phase 4 Actual Time: 16 hr/wk   
  
Name: Serdjan Rolovic  
EID: sr32452  
GitLab ID: slycane9  
Phase 1 Estimation Time: 15 hr/wk  
Phase 1 Actual Time: 14 hr/wk  
Phase 2 Estimation Time: 20 hr/wk  
Phase 2 Actual Time: 17.5 hr/wk   
Phase 3 Estimation Time: 17 hr/wk  
Phase 3 Actual Time: 16 hr/wk    
Phase 4 Estimation Time: 16 hr/wk   
Phase 4 Actual Time: 15 hr/wk   

Git SHA: e16e4a393089accaf8291e97d9aa58ca66d586e1    
  
GitLab Pipelines: https://gitlab.com/sanjanakapoor793/swe-team-4/pipelines
  
Website: https://www.politicianstakingmoney.me/ 

Comments:   
People worked together for some unit tests, so the test was counted for each person.  
