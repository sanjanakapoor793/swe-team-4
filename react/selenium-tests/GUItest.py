from sys import platform
import unittest
import time
from unittest import TestCase
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

class AcceptanceTests (TestCase):

	def endBrowser(self):
		self.browser.quit()

	def setUp(self):
		self.homeurl = 'https://politicianstakingmoney.me/'
		if platform == "linux" or platform == "linux2":
			self.browser = webdriver.Chrome('./drivers/chromedriver')
		self.browser.get(self.homeurl)

	# test default page
	def test1(self):
		self.browser.get(self.homeurl)
		self.assertIn('Politicians Taking Money', self.browser.title)
		self.endBrowser()

	# test that title link works
	def test2(self):
		try:
			self.browser.find_element_by_link_text('Politicians Taking Money').click()
			self.assertEqual("https://politicianstakingmoney.me/", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element doesn't exist")
		self.endBrowser()

	# test that politicians link works
	def test3(self):
		try:
			self.browser.find_element_by_link_text('Politicians').click()
			self.assertEqual("https://politicianstakingmoney.me/Politicians", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element doesn't exist")
		self.endBrowser()

	# test that donors link works
	def test4(self):
		try:
			self.browser.find_element_by_link_text('Donors').click()
			self.assertEqual("https://politicianstakingmoney.me/Donors", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element doesn't exist")
		self.endBrowser()

	# test that districts link works
	def test5(self):
		try:
			self.browser.find_element_by_link_text('Districts').click()
			self.assertEqual("https://politicianstakingmoney.me/Districts", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element doesn't exist")
		self.endBrowser()

	# test that about link works
	def test6(self):
		try:
			self.browser.find_element_by_link_text('About').click()
			self.assertEqual("https://politicianstakingmoney.me/About", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element doesn't exist")
		self.endBrowser()

	# test that home link works
	def test7(self):
		try:
			self.browser.find_element_by_link_text('Home').click()
			self.assertEqual("https://politicianstakingmoney.me/", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element doesn't exist")
		self.endBrowser()

	# test that link on about page works
	def test8(self):
		try:
			self.browser.find_element_by_link_text('About').click()
			self.browser.find_element_by_link_text('API').click()
			self.assertEqual("https://documenter.getpostman.com/view/6755256/S11KQeN3", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element doesn't exist")
		self.endBrowser()

	def test9(self):
		delay = 5
		try:
			self.browser.find_element_by_link_text('Donors').click()
			myElem = WebDriverWait(self.browser, delay).until(EC.element_to_be_clickable((By.CLASS_NAME, 'donorCard')))
			myElem.click()
			self.assertEqual("https://politicianstakingmoney.me/donor/Alliance%20to%20Save%20Energy", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element doesn't exist")
		self.endBrowser()

	def test10(self):
		delay = 5
		try:
			self.browser.find_element_by_link_text('Districts').click()
			myElem = WebDriverWait(self.browser, delay).until(EC.element_to_be_clickable((By.CLASS_NAME, 'districtCard')))
			myElem.click()
			self.assertEqual("https://politicianstakingmoney.me/district/Alabama-1", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element doesn't exist")
		self.endBrowser()

	def test11(self):
		try:
			element = self.browser.find_element_by_tag_name('input')
			element.send_keys("ralph")
			self.browser.find_element_by_link_text('Search').click()
			self.assertEqual("https://politicianstakingmoney.me/search/ralph?type=all", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element doesn't exist")
		self.endBrowser()

	def test12(self):
		delay = 5
		try:
			self.browser.find_element_by_link_text('Donors').click()
			element = WebDriverWait(self.browser, delay).until(EC.element_to_be_clickable((By.TAG_NAME, 'input')))
			element.click()
			element.send_keys("kidsave")
			self.browser.find_element_by_link_text('Search').click()
			element = WebDriverWait(self.browser, delay).until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, 'Kidsave')))
			element.click()
			self.assertEqual("https://politicianstakingmoney.me/donor/Kidsave", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element doesn't exist")
		self.endBrowser()

	def test13(self):
		delay = 5
		try:
			self.browser.find_element_by_link_text('Districts').click()
			element = WebDriverWait(self.browser, delay).until(EC.element_to_be_clickable((By.TAG_NAME, 'input')))
			element.click()
			element.send_keys("arizona")
			self.browser.find_element_by_link_text('Search').click()
			element = WebDriverWait(self.browser, delay).until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, 'Gilbert')))
			element.click()
			self.assertEqual("https://politicianstakingmoney.me/district/Arizona-5", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element doesn't exist")
		self.endBrowser()

	def test14(self):
		delay = 5
		try:
			self.browser.find_element_by_link_text('Politicians').click()
			element = WebDriverWait(self.browser, delay).until(EC.element_to_be_clickable((By.TAG_NAME, 'input')))
			element.click()
			element.send_keys("blunt")
			self.browser.find_element_by_link_text('Search').click()
			element = WebDriverWait(self.browser, delay).until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, 'Lisa Blunt')))
			element.click()
			self.assertEqual("https://politicianstakingmoney.me/politician?first=Lisa&last=Blunt%20Rochester", self.browser.current_url)
		except NoSuchElementException:
			self.fail("The element doesn't exist")
		self.endBrowser()

if __name__ == "__main__":
	unittest.main()

