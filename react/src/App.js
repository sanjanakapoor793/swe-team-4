import React, { Component } from 'react';
import MyNavbar from './components/MyNavbar';
import MyFooter from './components/MyFooter';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import './App.css';
import Home from './components/Home';
import About from './components/About';
import Model from './components/Model';
import Instance from './components/Instance';
import SearchResults from './components/SearchResults';
import Visualizations from './components/Visualizations';

class App extends Component {
  render() {
    return (
    	<Router>
	      <div className="App">
	        <MyNavbar/>
	        <div id='main'>
	            <Route exact path="/" component={Home} />
	            <Route exact path="/about" component={About} />
              <Route exact path="/visualizations" component={Visualizations} />
              <Route exact path="/search/:search" component={SearchResults} />
              <Route exact path="/politicians" render={(props) => <Model type='Politician' key='politicians' {...props}  />} />
              <Route exact path="/districts" render={(props) => <Model type='District' key='districts' {...props}/>} />
              <Route exact path="/donors" render={(props) => <Model type='Donor' key='donors' {...props}/>} />
              <Route exact path="/donor/:name" render={(props) => <Instance type='Donor' {...props}/>} />
              <Route exact path="/district/:name" render={(props) => <Instance type='District' {...props}/>} />
              <Route exact path="/politician" render={(props) => <Instance type='Politician' {...props}/>} />
	         </div>
	        <MyFooter/>
	      </div>
	    </Router>
    );
  }
}

export default App;
