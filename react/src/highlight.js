//Utility class to highlight relevent terms in search results
//Sourced from https://gist.github.com/evenfrost/1ba123656ded32fb7a0cd4651efd4db0
const highlight = (query, fuseSearchResult, highlightClassName = 'highlight') => {
    const set = (obj, path, value) => {
        const pathValue = path.split('.');
        let i;

        for (i = 0; i < pathValue.length - 1; i++) {
            obj = obj[pathValue[i]];
        }

        obj[pathValue[i]] = value;
    };

    const generateHighlightedText = (inputText, regions = []) => {
        let content = '';
        let nextUnhighlightedRegionStartingIndex = 0;
        let minBoldLength = Math.min(query.length, 2)
        query = query.toLowerCase();

        regions.forEach(region => {
            const lastRegionNextIndex = region[1] + 1;

            content +=
                (lastRegionNextIndex - region[0] < minBoldLength || !query.includes(inputText.substring(region[0], lastRegionNextIndex).toLowerCase())) ? inputText.substring(nextUnhighlightedRegionStartingIndex, lastRegionNextIndex):
                [
                inputText.substring(nextUnhighlightedRegionStartingIndex, region[0]),
                `<span class="${highlightClassName}">`,
                inputText.substring(region[0], lastRegionNextIndex),
                '</span>',
                ].join('');

            nextUnhighlightedRegionStartingIndex = lastRegionNextIndex;
        });

        content += inputText.substring(nextUnhighlightedRegionStartingIndex);

        return content;
    };

    return fuseSearchResult
        .filter(({ matches }) => matches && matches.length)
        .map(({ item, matches }) => {
            const highlightedItem = { ...item };

            matches.forEach((match) => {
                set(highlightedItem, match.key, generateHighlightedText(match.value, match.indices));
            });
            return highlightedItem;
        });
};

export default highlight;