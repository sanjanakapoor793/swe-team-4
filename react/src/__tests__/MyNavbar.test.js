import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router} from 'react-router-dom';
import MyNavbar from '../components/MyNavbar';

it('renders without crashing', () => {
	const div = document.createElement('div');
  	ReactDOM.render(<Router><MyNavbar/></Router>, div);
  	ReactDOM.unmountComponentAtNode(div);
});