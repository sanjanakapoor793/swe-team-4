import React from 'react';
import ReactDOM from 'react-dom';
import Model from '../components/Model';

it('renders without crashing', () => {
	const div = document.createElement('div');
  	ReactDOM.render(<Model type={"politicians"}/>, div);
  	ReactDOM.unmountComponentAtNode(div);
});