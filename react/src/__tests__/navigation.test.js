import React from 'react';
import {withRouter} from 'react-router';
import {render, fireEvent, cleanup} from 'react-testing-library';
import {Link, Route, Router, Switch} from 'react-router-dom';
import {createMemoryHistory} from 'history';
import ReactDOM from 'react-dom';
import About from '../components/About';
import App from '../App';
import Politicians from '../components/Model';

afterEach(cleanup)

function renderWithRouter(
	ui, {route = '/', history = createMemoryHistory({initialEntries: [route]})} = {},
) {
	return {
		...render(<Router history={history}>{ui}</Router>),
		history,
	}
}

test('full app rendering/navigating', () => {
	const {container, getByText} = renderWithRouter(<App />)
	expect(container.innerHTML).toMatch('Politicians Taking Money')
	const leftclick = {a: 5}
	fireEvent.click(getByText(/About/i), leftclick)
	expect(container.innerHTML).toMatch('About This Site')
});

test('navigating2', () => {
	const {container, getByText} = renderWithRouter(<App />)
	expect(container.innerHTML).toMatch('Politicians Taking Money')
	const leftclick = {a: 3}
	fireEvent.click(getByText(/Donors/i), leftclick)
	expect(container.innerHTML).toMatch('Donors')
});

test('navigating3', () => {
	const {container, getByText} = renderWithRouter(<App />)
	expect(container.innerHTML).toMatch('Politicians Taking Money')
	const leftclick = {a: 4}
	fireEvent.click(getByText(/Districts/i), leftclick)
	expect(container.innerHTML).toMatch('Districts')
});

test('navigating4', () => {
	const {container, getByText} = renderWithRouter(<App />)
	expect(container.innerHTML).toMatch('Politicians Taking Money')
	const leftclick = {a: 'f2'}
	fireEvent.click(getByText(/Donors/i), leftclick)
	expect(container.innerHTML).toMatch('Donors')
});

test('navigating5', () => {
	const {container, getByText} = renderWithRouter(<App />)
	expect(container.innerHTML).toMatch('Politicians Taking Money')
	const leftclick = {a: '2'}
	fireEvent.click(getByText(/Politicians/i), leftclick)
	expect(container.innerHTML).toMatch('Politicians')
});