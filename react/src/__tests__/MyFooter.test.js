import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router} from 'react-router-dom';
import MyFooter from '../components/MyFooter';

it('renders without crashing', () => {
	const div = document.createElement('div');
  	ReactDOM.render(<Router><MyFooter/></Router>, div);
  	ReactDOM.unmountComponentAtNode(div);
});