import React, { Component } from 'react';
import {Container, Row} from 'react-bootstrap'
import {NavLink} from 'react-router-dom';
class MyFooter extends Component {
  render() {
    return (
        <footer className='fixed-bottom'>
            <Container className='flex-content'>
                <Row>
                    <NavLink id="f0" style={{padding: '6px', fontsize: '18px'}} to="/">Home</NavLink>
                    <NavLink id="f1" style={{padding: '6px', fontsize: '18px'}} to="/Politicians">Politicians</NavLink>
                    <NavLink id="f2" style={{padding: '6px', fontsize: '18px'}} to="/Donors">Donors</NavLink>
                    <NavLink id="f3" style={{padding: '6px', fontsize: '18px'}} to="/Districts">Districts</NavLink>
                    <NavLink id="f4" style={{padding: '6px', fontsize: '18px'}} to="/About">About</NavLink>
                </Row>
            </Container>
        </footer>
    );
  }
}

export default MyFooter;