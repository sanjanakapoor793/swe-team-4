import React, { Component } from 'react';
import { API_URL } from './Model';
import { ResponsiveContainer, Scatter, ScatterChart, Legend, Text, PieChart, Pie, Label, Cell, BarChart, CartesianGrid, XAxis, YAxis, Tooltip, Bar, Radar, RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis } from "recharts";
import { Container } from 'react-bootstrap';

import DefaultTooltipContent from 'recharts/lib/component/DefaultTooltipContent';

const dev_API_URL = "https://api.booksandbones.me";

const CustomTooltip = props => {
  // we don't need to check payload[0] as there's a better prop for this purpose
  if (!props.active) {
    // I think returning null works based on this: http://recharts.org/en-US/examples/CustomContentOfTooltip
    return null
  }
  // mutating props directly is against react's conventions
  // so we create a new payload with the name and value fields set to what we want
  const newPayload = [
    {
      name: 'Name',
      // all your data which created the tooltip is located in the .payload property
      value: props.payload[0].payload.name,
      // you can also add "unit" here if you need it
    },
    ...props.payload,
  ];

  // we render the default, but with our overridden payload
  return <DefaultTooltipContent {...props} payload={newPayload} />;
};

class ScatterTooltip extends Component {
  //type payload label
  render() {
    const { active } = this.props;

    if (active) {
      const { payload, label } = this.props;
      return (
        <div className="custom-tooltip">
          <p className="label">{`${label} : ${payload[0].value}`}</p>
          <p className="intro">{}</p>
          <p className="desc">Anything you want can be displayed here.</p>
        </div>
      );
    }

    return null;
  }
}

class Visualizations extends Component {
  constructor(props) {
    super(props);
    this.state = {
      piedata:
        [
          { label: 'Male', value: 0 },
          { label: 'Female', value: 0 }
        ],
      devpiedata:
        [
          { label: 'Male', value: 0 },
          { label: 'Female', value: 0 }
        ],
      bardata: [],
      devbardata: [],
      scatterdata: [],
      devradardata: []
    };

  }

  getScatterData() {
    const URL = `${API_URL}/districts`;
    let data = [];

    fetch(URL).then(r => r.json())
      .then(r => {
        r.map(e => {
          data.push({name: e.name, income: e.median_income, population: e.population});
        });
        this.setState({ scatterdata: data });
      });
  }

  getBarData() {
    const URL = `${API_URL}/donors`;
    let data = [];

    fetch(URL).then(r => r.json())
      .then(r => {
        r.map(e => {
          if(e.repparty + e.demparty > 50000)
            data.push({name: e.name, rep: e.repparty, dem: e.demparty});
        });
        this.setState({ bardata: data });
      });
  }

  getPieData() {
    const URL = `${API_URL}/politicians`;
    let m = 0;
    let f = 0;

    fetch(URL).then(r => r.json())
      .then(r => {
        r.map(e => {
          if (e.gender === 'M')
            m++;
          else
            f++;
        });
        this.setState({ piedata: [{ label: 'Male', value: m }, { label: 'Female', value: f }] });
      });
  }

  // Dev Graphs

  getDevBarData() {
    const URL = `${dev_API_URL}/museums/?limit=9999`;
    let data = [];

    fetch(URL).then(r => r.json())
      .then(r => {
        //console.log(r);
        r.results.map(e => {
            let search = data.filter(d => d.name === e.contact_info[0].address_county);
            if (search.length > 0)
              search[0].value++;
            else
              data.push({name: e.contact_info[0].address_county, value: 1});
        });
        //console.log(data);
        this.setState({ devbardata: data });
      });
  }

  getDevPieData() {
    const URL1 = `${dev_API_URL}/museums`;
    const URL2 = `${dev_API_URL}/libraries`;
    let m = 0;
    let l = 0;

    fetch(URL1).then(r => r.json())
      .then(r => {
        m = r.count;
        this.setState({ devpiedata: [{ label: 'Museums', value: m }, { label: 'Libraries', value: l }] });

      });

    fetch(URL2).then(r => r.json())
      .then(r => {
        l = r.count;
        this.setState({ devpiedata: [{ label: 'Museums', value: m }, { label: 'Libraries', value: l }] });
      });
  }

  getDevRadarData() {
    const URL = `${dev_API_URL}/libraries/?limit=9999`;
    let data = [];

    fetch(URL).then(r => r.json())
      .then(r => {
        //console.log(r);
        r.results.map(e => {
              data.push({name: e.name, value: e.events.length});
        });
        //console.log(data);
        data.sort(function(a, b) {
            return a.value - b.value;
        });
        this.setState({ devradardata: data });
      });
    
  }

  componentDidMount() {
    this.getPieData();
    this.getBarData();
    this.getScatterData();

    this.getDevBarData();
    this.getDevPieData();
    this.getDevRadarData();
  }

  drawScatter() {
    const data = this.state.scatterdata;
    return (
      <ResponsiveContainer width="100%" height={750}>
        <ScatterChart height={500} margin={{top: 20, right: 20, bottom: 20, left: 20}}>
          <CartesianGrid />
          <XAxis dataKey={'income'} type="number" name='Income' height={50}>
            <Label value="Median Income ($)" offset={0} position="insideBottom"/>
          </XAxis>
          <YAxis label={{ value: 'Population', angle: -90, position:"insideLeft" }} width={100} dataKey={'population'} type="number" name='Population'/>
          <Scatter name='A school' data={data} fill='#8884d8'/>
          <Tooltip content={<CustomTooltip />} cursor={{strokeDasharray: '3 3'}}/>
        </ScatterChart>
      </ResponsiveContainer>
    );
  }

  drawBar() {
    const data = this.state.bardata;
    return (
      <ResponsiveContainer width="100%" height={750}>
        <BarChart height={300} data={data}
              margin={{top: 5, right: 30, left: 20, bottom: 5}}>
          <Text value="Partisan Donations by Donor" position="top"/>
          <CartesianGrid strokeDasharray="3 3"/>
          <XAxis dataKey="name" angle={60} textAnchor="start" height={300} minTickGap={1} interval={0}/>
          <YAxis/>
          <Tooltip/>
          <Legend />
          <Bar dataKey="rep" name="Republican Party" stackId='a' fill="#E9141D" />
          <Bar dataKey="dem" name="Democratic Party" stackId='a' fill="#0015BC" />
        </BarChart>
      </ResponsiveContainer>
    );
  }

  drawPie() {
    const data = this.state.piedata;
    return (
      <ResponsiveContainer width="100%" height={500}>
        <PieChart height={250}>
          <Pie
            data={data}
            cx="50%"
            cy="50%"
            outerRadius={200}
            innerRadius={75}
            fill={'#FFFFFF'}
            dataKey="value"
            label={({
              cx,
              cy,
              midAngle,
              innerRadius,
              outerRadius,
              value,
              index
            }) => {
              const RADIAN = Math.PI / 180;
              // eslint-disable-next-line
              const radius = 25 + innerRadius + (outerRadius - innerRadius);
              // eslint-disable-next-line
              const x = cx + radius * Math.cos(-midAngle * RADIAN);
              // eslint-disable-next-line
              const y = cy + radius * Math.sin(-midAngle * RADIAN);

              return (
                <text
                  x={x}
                  y={y}
                  fill={index === 0 ? "#4286f4" : "#ff5454"}
                  textAnchor={x > cx ? "start" : "end"}
                  dominantBaseline="central"
                >
                  {data[index].label} ({value})
                  }
              </text>
              );
            }}
          >
            {data.map((entry, index) => <Cell key={index} fill={index === 0 ? "#4286f4" : "#ff5454"}/>)}
          </Pie>
        </PieChart>
      </ResponsiveContainer>
    );
  }

  // bar graph of number of museums per county

  // bubble of  number of events per library

  // pie chart number of libraries vs museums
  drawDevBar() {
    const data = this.state.devbardata;
    return (
      <ResponsiveContainer width="100%" height={750}>
        <BarChart height={300} data={data}
              margin={{top: 5, right: 30, left: 20, bottom: 5}}>
          <Text value="Number of Events by City" position="top"/>
          <CartesianGrid strokeDasharray="3 3"/>
          <XAxis dataKey="name" angle={60} textAnchor="start" height={300} minTickGap={1} interval={0}/>
          <YAxis/>
          <Tooltip/>
          <Legend />
          <Bar dataKey="value" name="Number of Museums" stackId='a' fill="#E9141D" />
        </BarChart>
      </ResponsiveContainer>
    );
  }

  drawDevPie() {
    const data = this.state.devpiedata;
    return (
      <ResponsiveContainer width="100%" height={500}>
        <PieChart height={250}>
          <Pie
            data={data}
            cx="50%"
            cy="50%"
            outerRadius={200}
            innerRadius={75}
            fill={'#FFFFFF'}
            dataKey="value"
            label={({
              cx,
              cy,
              midAngle,
              innerRadius,
              outerRadius,
              value,
              index
            }) => {
              const RADIAN = Math.PI / 180;
              // eslint-disable-next-line
              const radius = 25 + innerRadius + (outerRadius - innerRadius);
              // eslint-disable-next-line
              const x = cx + radius * Math.cos(-midAngle * RADIAN);
              // eslint-disable-next-line
              const y = cy + radius * Math.sin(-midAngle * RADIAN);

              return (
                <text
                  x={x}
                  y={y}
                  fill={index === 0 ? "#4286f4" : "#ff5454"}
                  textAnchor={x > cx ? "start" : "end"}
                  dominantBaseline="central"
                >
                  {data[index].label} ({value})
                  }
              </text>
              );
            }}
          >
            {data.map((entry, index) => <Cell key={index} fill={index === 0 ? "#4286f4" : "#ff5454"}/>)}
          </Pie>
        </PieChart>
      </ResponsiveContainer>
    );
  }

  drawDevRadar() {
    const data = this.state.devradardata;

      return (
        <ResponsiveContainer width="100%" height={500}>
          <RadarChart height={500} data={data}>
            <PolarGrid />
            <PolarAngleAxis dataKey="name" />
            <PolarRadiusAxis />
            <Radar name="name" dataKey="value" stroke="#8884d8" fill="#8884d8" fillOpacity={0.6} />
          </RadarChart>
        </ResponsiveContainer>
      );
  }

  render() {
    return (
      <Container style={{ height: '100%' }}>
        <h1>Visualizations</h1>
        <hr/>
        <h3># Of Men vs Women in the House</h3>
        {this.drawPie()}
        <hr/>
        <h3>Partisan Donations by Donor Over $50,000</h3>
        {this.drawBar()}
        <hr/>
        <h3 style={{paddingTop:'20px'}}>Population vs Median Income by District</h3>
        {this.drawScatter()}

        <hr/>
        <h3> Developer Visualizations </h3>
        <hr/>

        <h3>Number of Museums per County</h3>
        {this.drawDevBar()}
        <hr/>
        <h3>Number of Museums vs Libraries in Austin</h3>
        {this.drawDevPie()}
        <hr/>
        <h3>Number of Events per Museum</h3>
        {this.drawDevRadar()}
      </Container>
    );
  }
}

export default Visualizations;