import React, { Component } from 'react';
import {Table, Row, Col} from 'react-bootstrap';
import Chart from "react-google-charts";
import { API_URL } from './Model';
import {Link} from 'react-router-dom';

const pieOptions = {
  title: "",
  slices: [
    {
      color: "#2BB673"
    },
    {
      color: "#d91e48"
    },
    {
      color: "#007fad"
    },
    {
      color: "#e9a227"
    },
    {
      color: "#66ff66"
    }
  ],
  legend: {
    position: "right",
    alignment: "center",
    textStyle: {
      color: "233238",
      fontSize: 14
    }
  },
  tooltip: {
    showColorCode: true
  },
  chartArea: {
    left: 0,
    top: 50,
    bottom: 25,
    width: "100%",
    height: "80%"
  },
  fontName: "Roboto",
  fontSize: 20,
  sliceVisibilityThreshold: 1/1000
};

function strToNum(str) {
	var arr1 = str.split(", ");
	var arr2 = [];
	for (var i = 0; i < arr1.length; ++i) {
		var temp = arr1[i].split("%");
		arr2.push(parseFloat(temp[0]));
	}
	return arr2;
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

class DistrictInstance extends Component {
	constructor(props) {
		super(props);
		this.state = {
			politicianLink: false,
			donorLink: false
		};
	}

	componentDidMount() {
		let pname = this.props.data.reps;
		let split = pname.split(' ');
		let purl = `${API_URL}/politician?first=${split[0]}&last=${split[1]}`;
		fetch(purl).then(res => {
				if(res.status === 200)
						this.setState({politicianLink: true});
		});

		let dname = this.props.data.donors;
		let durl = `${API_URL}/donor/${dname}`;
		fetch(durl).then(res => {
				if(res.status === 200)
						this.setState({donorLink: true});
		});
	}

	renderPolitician(name){
		if(name === null)
			return (<td>"N/A"</td>);
		else if(!this.state.politicianLink)
			return (<td>{name}</td>);
		else
			return (<td><Link to={`/politician?first=${name.split(' ')[0]}&last=${name.split(' ')[1]}`}>{name}</Link></td>);
	}

	renderDonor(name){
		if(name === null)
			return (<td>"N/A"</td>);
		else if(!this.state.donorLink)
			return (<td>{name}</td>);
		else
			return (<td><Link to={'/donor/'+name}>{name}</Link></td>);
	}

  render() {
    let data = this.props.data;
    const intPop = strToNum(data.ethnicity);
    var intLoc = null;
    if (data.distribution != null) {
    	intLoc = strToNum(data.distribution);
	}
    return (
		<div>
			<Row>
				<Col sm={5}>
					<section className="instance">
						<div className="container">
					        <img className="img-fluid center-block" src={data.imageurl} alt=""/> 
					    </div>
					    <hr/>
					    <h3 className="text-center">{data.name}</h3>
				    </section>
				</Col>
				<Col sm={3}>
				    <section className="district info">
				      	<div className="container">
					        <Table bordered>
					          	<thead>
						            <tr>
						              	<th scope="col">Attribute</th>
						              	<th scope="col">Information</th>
						            </tr>
					          	</thead>
					          	<tbody>
					          		<tr>
							            <td>Representative</td>
					          			{this.renderPolitician(data.reps)}
							        	</tr>
		                            <tr>
							            <td>Population</td>
					          			{data.population ? <td>{data.population}</td> : "N/A"}
							        </tr>
							        <tr>
							            <td>Major City</td>
					          			{data.majorcity ? <td>{data.majorcity}</td> : "N/A"}
							        </tr>
							        <tr>
							            <td>Major Donor</td>
					          			{this.renderDonor(data.donors)}
							        </tr>
							        <tr>
							            <td>Median Income</td>
					          			{data.donors ? <td>${numberWithCommas(data.median_income)}</td> : "N/A"}
							        </tr>
					          	</tbody>
					        </Table>
				      	</div>
				    </section>
				</Col>
				<Col sm={4}>
					<Chart
				        chartType="PieChart"
				        data={[["Ethnicity", "Percentage"],
				        ["White", intPop[0]], ["Black", intPop[1]], ["Asian", intPop[2]], ["Hispanic", intPop[3]], ["Native", intPop[4]]]}
				        options={pieOptions}
				        graph_id="PieChart1"
				        width={"100%"}
				        height={"200px"}
				        legend_toggle
			        />
			        {intLoc ?
			        <Chart
				        chartType="PieChart"
				        data={[["Distribution", "Percentage"],
				        ["Urban", intLoc[0]], ["Rural", intLoc[1]]]}
				        options={pieOptions}
				        graph_id="PieChart2"
				        width={"100%"}
				        height={"200px"}
				        legend_toggle
			        />
			        : null}
			    </Col>
			</Row>
		</div>
    );
  }
}

export default DistrictInstance;