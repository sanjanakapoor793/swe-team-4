import React, { Component } from 'react';
import {Table, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {TwitterTimelineEmbed} from 'react-twitter-embed';
import {SocialIcon} from 'react-social-icons';
import { API_URL } from './Model';

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

class DonorInstance extends Component {
	constructor(props) {
		super(props);
		this.state = {
			districtLink: false,
			politicianLink: false
		};
	}

	componentDidMount() {
		let pname = this.props.data.rep;
		let split = pname.split(' ');
		let purl = `${API_URL}/politician?first=${split[0]}&last=${split[1]}`;
		fetch(purl).then(res => {
				if(res.status === 200)
						this.setState({politicianLink: true});
		});

		let dname = this.props.data.district;
		let durl = `${API_URL}/district/${dname}`;
		fetch(durl).then(res => {
				if(res.status === 200)
						this.setState({districtLink: true});
		});
	}

	renderPolitician(name){
		if(name === null)
			return (<td>"N/A"</td>);
		else if(!this.state.politicianLink)
			return (<td>{name}</td>);
		else
			return (<td><Link to={`/politician?first=${name.split(' ')[0]}&last=${name.split(' ')[1]}`}>{name}</Link></td>);
	}

	renderDistrict(name){
		if(name === null)
			return (<td>"N/A"</td>);
		else if(!this.state.districtLink)
			return (<td>{name}</td>);
		else
			return (<td><Link to={`/district/${name}`}>{name}</Link></td>);
	}

  render() {
    let data = this.props.data;
    return (
		<div>
			<Row>
				<Col sm={3}>
					
						<div className="container">
					        <img className="img-fluid center-block" src={data.logourl} alt=""/> 
					    </div>
					    <hr/>
					    <a href={'https://' + data.website}><h3 className="text-center">{data.name}</h3></a>
				    
				    <hr/>
				    
				      	<div className="container">
					        <Table bordered>
					          	<thead>
						            <tr>
						              	<th scope="col">Attribute</th>
						              	<th scope="col">Info</th>
						            </tr>
					          	</thead>
					          	<tbody>
					          		<tr>
		                            	<td>Home City</td>
		                            	<td>{data.city}, {data.country}</td>
		                            </tr>
		                            <tr>
		                            	<td>Industry</td>
		                            	<td>{data.industry}</td>
		                            </tr>
		                            <tr>
		                            	<td>Phone Number</td>
		                            	<td>{data.phone_num}</td>
		                            </tr>
																<tr>
		                            	<td>Representative</td>
		                            	{this.renderPolitician(this.props.data.rep)}
		                            </tr>
																<tr>
		                            	<td>District</td>
		                            	{this.renderDistrict(this.props.data.district)}
		                            </tr>
					          	</tbody>
					        </Table>
					        <hr/>
				      	</div>

				</Col>

			    <Col sm={4}>
				    <section className="donation info">
				      	<div className="container">
					        <Table bordered>
					          	<thead>
						            <tr>
						              	<th scope="col">Donations To</th>
						              	<th scope="col">Amount</th>
						            </tr>
					          	</thead>
					          	<tbody>
					          		<tr>
							            <td>Democrat Donations</td>
					          			{data.demparty ? <td>${numberWithCommas(data.demparty)}</td> : "$0"}
							        </tr>
		                            <tr>
							            <td>Republican Donations</td>
					          			{data.repparty ? <td>${numberWithCommas(data.repparty)}</td> : "$0"}
							        </tr>
							        <tr>
							            <td>Individual Donations</td>
					          			{data.indivdonate ? <td>${numberWithCommas(data.indivdonate)}</td> : "$0"}
							        </tr>
							        <tr>
							            <td>Pac Donations</td>
					          			{data.pacsdonate ? <td>${numberWithCommas(data.pacsdonate)}</td> : "$0"}
							        </tr>
					          	</tbody>
					        </Table>
				      	</div>
				    </section>
				    <SocialIcon className="tw" style={{margin: 25}} url={"http://twitter.com/" + data.twitter}/>
				    <SocialIcon className="fb" style={{margin: 25}} url={data.facebook}/>
				</Col>
				<Col>
				<div className="centerContent">
			    	<div className="selfCenter standardWidth">
			    		<TwitterTimelineEmbed
			    			sourceType="profile"
			    			screenName={data.twitter ? data.twitter : ""}
			    			options={{height: 600, width: 400, valign: 'top'}}	
			    		/>
			    	</div>
			    </div>
				</Col>
			</Row>
		</div>
    );
  }
}

export default DonorInstance;