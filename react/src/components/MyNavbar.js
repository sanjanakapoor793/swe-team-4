import React, { Component } from 'react';
import {Navbar, Nav, Container} from 'react-bootstrap';
import {NavLink} from 'react-router-dom';
class MyNavbar extends Component {
  render() {
    return (
			<Navbar bg="light" expand="lg">
				<Container>
					<Navbar.Brand><NavLink id="0" to="/">Politicians Taking Money</NavLink></Navbar.Brand>
					<Navbar.Toggle aria-controls="basic-navbar-nav" />
					<Navbar.Collapse id="basic-navbar-nav">
						<Nav className="ml-auto p-0">
							<NavLink id="1" to="/">Home</NavLink>
							<NavLink id="2" to="/Politicians">Politicians</NavLink>
							<NavLink id="3" to="/Donors">Donors</NavLink>
							<NavLink id="4" to="/Districts">Districts</NavLink>
							<NavLink id="5" to="/About">About</NavLink>
							<NavLink to="/Visualizations">Visualizations</NavLink>
						</Nav>
					</Navbar.Collapse>
				</Container>	
			</Navbar>
		
    );
  }
}

export default MyNavbar;
