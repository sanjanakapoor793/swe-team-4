import React, { Component } from 'react';
import { Col, Row, Table, Card } from 'react-bootstrap';

const PROJECTID = 10903935;
const ISSUESURL = `https://gitlab.com/api/v4/projects/${PROJECTID}/issues?per_page=999`;
const COMMITSURL = `https://gitlab.com/api/v4/projects/${PROJECTID}/repository/contributors`;

/* Main component for the home page */
class About extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userCommits: {
        aj: 0,
        sr: 0,
        ln: 0,
        sk: 0,
        cb: 0,
        total: 0
      },
      userIssues: {
        aj: 0,
        sr: 0,
        ln: 0,
        sk: 0,
        cb: 0,
        total: 0
      }
    }
  }

  displayCommits = (data) => {
    let userCommits =
    {
      aj: 0,
      sr: 0,
      ln: 0,
      sk: 0,
      cb: 0,
      total: 0
    };

    for (let i = 0; i < data.length; i++) {
      switch (data[i].name) {
        case 'Aaron Johnson':
          userCommits.aj += data[i].commits;
          break;
        case 'Clinton Bell':
          userCommits.cb += data[i].commits;
          break;
        case 'Lyndon Nguyen':
        case 'LB Nguyen':
        case 'lbn99':
          userCommits.ln += data[i].commits;
          break;
        case 'Sanjana Kapoor':
          userCommits.sk += data[i].commits;
          break;
        case 'Serdjan Rolovic':
          userCommits.sr += data[i].commits;
          break;
        default:
          break;
      }
      userCommits.total += data[i].commits;
    }

    this.setState({ userCommits: userCommits });

  }

  displayIssues = (data) => {
    let userIssues =
    {
      aj: 0,
      sr: 0,
      ln: 0,
      sk: 0,
      cb: 0,
      total: 0
    };

    for (let i = 0; i < data.length; i++) {
      switch (data[i].author.username) {
        case 'aarjo':
          userIssues.aj++;
          break;
        case 'bec7992':
          userIssues.cb++;
          break;
        case 'lbn99':
          userIssues.ln++;
          break;
        case 'slycane9':
          userIssues.sr++;
          break;
        case 'sanjanakapoor793':
          userIssues.sk++;
          break;
        default:
          break;
      }
      userIssues.total++;
    }

    this.setState({ userIssues: userIssues });
  }

  componentDidMount() {
    fetch(COMMITSURL).then((res) => res.json().then(this.displayCommits));
    fetch(ISSUESURL).then((res) => res.json().then(this.displayIssues));
  }
  render() {
    let commits = this.state.userCommits;
    let issues = this.state.userIssues;
    return (
      <div className="container">
        <div align="center">
          <h2>About This Site</h2>
          <p>
            The purpose of this site is to spread awareness of how money plays into politics.
            We want to help voters make informed decisions on who to support in future campaigns
            based on their track record with certain companies/industries in their district.
          </p>
        </div>

        <div align="center">
          <h2>Analyzing the Result</h2>
          <p>
            Integrating disparate data allows voters to quickly and easily see who politicians are
            accepting money from. Voters will be able to see who is donating money to their
            representative and will be ably to easily detect existing patterns between politicians
            and donors.
          </p>
        </div>

        <hr />
          <Row as="section" className="bios" style={{justifyContent:"center", margin:"-10px 0 0 0"}}>
            <Col md={4} sm={12}>
              <Card className="card-widget">
                <Card.Img className="aboutimg align-center" variant="top" src="img/cb-id.jpg" />
                <Card.Body>
                  <h4>Clinton Bell</h4>
                  <Card.Subtitle>Frontend Engineer</Card.Subtitle>
                  <Card.Text>Computer Science student at UT Austin</Card.Text>
                </Card.Body>
              </Card>
            </Col>

            <Col md={4} sm={12}>
              <Card className="card-widget img-fluid">
                <Card.Img className="aboutimg" variant="top" src="img/aj-id2.JPG" />
                <Card.Body className="d-flex flex-column">
                  <h4  className="mt-auto">Aaron Johnson</h4>
                  <Card.Subtitle>Frontend Engineer</Card.Subtitle>
                  <Card.Text>Sophomore at UTCS </Card.Text>
                </Card.Body>
              </Card>
            </Col>

            <Col md={4} sm={12}>
              <Card className="card-widget img-fluid">
                <Card.Img className="aboutimg" variant="top" src="img/sr-id.jpg" />
                <Card.Body>
                  <h4>Serdjan Rolovic</h4>
                  <Card.Subtitle>Backend Engineer</Card.Subtitle>
                  <Card.Text>Sophomore at UTCS </Card.Text>
                </Card.Body>
              </Card>
            </Col>

            <Col md={4} sm={12}>
              <Card className="card-widget img-fluid">
                <Card.Img className="aboutimg" variant="top" src="img/ln-id.jpg" />
                <Card.Body>
                  <h4>Lyndon Nguyen</h4>
                  <Card.Subtitle>Backend Engineer</Card.Subtitle>
                  <Card.Text>CS sophomore who enjoys games, basketball, and noodle soups</Card.Text>
                </Card.Body>
              </Card>
            </Col>

            <Col md={4} sm={12}>
              <Card className="card-widget img-fluid">
                <Card.Img className="aboutimg" variant="top" src="img/sk-id.jpeg" />
                <Card.Body>
                  <h4>Sanjana Kapoor</h4>
                  <Card.Subtitle>Backend Engineer</Card.Subtitle>
                  <Card.Text>Sophomore at UTCS who likes to read</Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>

        <div align="center">
          <h2>Statistics</h2>
        </div>

        <Table bordered>
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Commits</th>
              <th scope="col">Issues</th>
              <th scope="col">Unit Tests</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">Clinton</th>
              <td><span id='cbcommits'>{commits.cb}</span></td>
              <td><span id='cbissues'>{issues.cb}</span></td>
              <td><span id='cbut'>14</span></td>
            </tr>
            <tr>
              <th scope="row">Aaron</th>
              <td><span id='ajcommits'>{commits.aj}</span></td>
              <td><span id='ajissues'>{issues.aj}</span></td>
              <td><span id='ajut'>11</span></td>
            </tr>
            <tr>
              <th scope="row">Sanjana</th>
              <td><span id='skcommits'>{commits.sk}</span></td>
              <td><span id='skissues'>{issues.sk}</span></td>
              <td><span id='skut'>10</span></td>
            </tr>
            <tr>
              <th scope="row">Serdjan</th>
              <td><span id='srcommits'>{commits.sr}</span></td>
              <td><span id='srissues'>{issues.sr}</span></td>
              <td><span id='srut'>20</span></td>
            </tr>
            <tr>
              <th scope="row">Lyndon</th>
              <td><span id='lncommits'>{commits.ln}</span></td>
              <td><span id='lnissues'>{issues.ln}</span></td>
              <td><span id='lnut'>16</span></td>
            </tr>
            <tr>
              <th scope="row">Total</th>
              <td><span id='tcommits'>{commits.total}</span></td>
              <td><span id='tissues'>{issues.total}</span></td>
              <td><span id='tut'>71</span></td>
            </tr>
          </tbody>
        </Table>

        <hr />

        <div align="center">
          <h2>Data</h2>

          <h4>Data Links</h4>
          <h5>
            <a href="https://www.propublica.org/datastore/api/campaign-finance-api">Campaign Finance API</a><br />
            <a href="https://developers.google.com/civic-information/">Civic Information API</a><br />
            <a href="https://api.open.fec.gov/developers/">OpenFEC API</a><br />
            <a href="https://www.opensecrets.org/open-data/api">OpenSecrets' API</a><br />
          </h5>

          <br />
          <br />

          <h4>Tools Used</h4>
          <h5>
            Postman - used to design our RESTful API <br />
            AWS - used to host our website <br />
            GitLab - used to store code / version control <br />
            NameCheap - used to get the URL for the website <br />
            Slack - used to communicate with team <br />
          </h5>
        </div>

        <hr />

        <h3 align="center">
          GitLab URL
                    <br />
          <a href="https://gitlab.com/sanjanakapoor793/swe-team-4">Politicians Taking Money</a>
        </h3>

        <hr />

        <h3 align="center">
          Postman URL
                    <br />
          <a href="https://documenter.getpostman.com/view/6755256/S11KQeN3">API</a>
        </h3>
        <script src={'js/about.js'}></script>
      </div>
    );
  }
}

export default About;
