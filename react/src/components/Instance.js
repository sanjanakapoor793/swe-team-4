import React, { Component } from 'react';
import PoliticianInstance from './PoliticianInstance';
import DistrictInstance from './DistrictInstance';
import DonorInstance from './DonorInstance'
import {API_URL} from './Model'

class Instance extends Component {
	constructor(props) {
		super(props);
		this.state = {
			data: null
		}
	}

	componentDidMount(){
		const { match: { params } } = this.props;
		let endpoint = params.name ? "/" + params.name : "";
		
		let url = `${API_URL}/${this.props.type.toLowerCase()}${endpoint}${this.props.location.search}`;

		let myRequest = new Request(url);

		fetch(myRequest)
		.then(response => response.json())
		.then(data => this.setState({data: data}));
	}

  render() {
    return (
			this.state.data != null ?
			(this.props.type === 'Politician' ? <PoliticianInstance data={this.state.data} /> :
			this.props.type === 'Donor' ? <DonorInstance data={this.state.data} /> :
			this.props.type === 'District' ? <DistrictInstance data={this.state.data} /> :
			<h1>Not Found</h1>)
			: <p>Loading</p>
    );
  }
}

export default Instance;