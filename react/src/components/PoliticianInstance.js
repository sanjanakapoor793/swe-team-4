import React, { Component } from 'react';
import {Table, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {TwitterTimelineEmbed} from 'react-twitter-embed';
import {SocialIcon} from 'react-social-icons';
import { API_URL } from './Model';

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

class PoliticianInstance extends Component {
	constructor(props) {
		super(props);
		this.state = {
			districtLink: false,
			donor1Link: false,
			donor2Link: false,
			donor3Link: false,
			donor4Link: false,
			donor5Link: false
		};
	}

	componentDidMount() {
		let d1name = this.props.data.donorOneName;
		let d1url = `${API_URL}/donor/${d1name}`;
		fetch(d1url).then(res => {
				if(res.status === 200)
						this.setState({donor1Link: true});
		});

		let d2name = this.props.data.donorTwoName;
		let d2url = `${API_URL}/donor/${d2name}`;
		fetch(d2url).then(res => {
				if(res.status === 200)
						this.setState({donor2Link: true});
		});

		let d3name = this.props.data.donorThreeName;
		let d3url = `${API_URL}/donor/${d3name}`;
		fetch(d3url).then(res => {
				if(res.status === 200)
						this.setState({donor3Link: true});
		});

		let d4name = this.props.data.donorFourName;
		let d4url = `${API_URL}/donor/${d4name}`;
		fetch(d4url).then(res => {
				if(res.status === 200)
						this.setState({donor4Link: true});
		});

		let d5name = this.props.data.donorFiveName;
		let d5url = `${API_URL}/donor/${d5name}`;
		fetch(d5url).then(res => {
				if(res.status === 200)
						this.setState({donor5Link: true});
		});

		let districtname = convertRegion(this.props.data.state, TO_NAME) + "-" + this.props.data.district;
		let districturl = `${API_URL}/district/${districtname}`;
		fetch(districturl).then(res => {
				if(res.status === 200)
						this.setState({districtLink: true});
		});
	}

	renderDistrict(){
		let name = convertRegion(this.props.data.state, TO_NAME) + "-" + this.props.data.district;
		if(name === null)
			return (<td>"N/A"</td>);
		else if(!this.state.districtLink)
			return (<td>{name}</td>);
		else
			return (<td><Link to={`/district/${name}`}>{name}</Link></td>);
	}

	renderDonor(name, which){
		if(name === null)
			return (<td>"N/A"</td>);
		else if(!this.state[which])
			return (<td>{name}</td>);
		else
			return (<td><Link to={'/donor/'+name}>{name}</Link></td>);
	}

  render() {
    let data = this.props.data;
    return (
		<div>
			<Row>
				<Col sm={3}>
					
					<div className="container">
				        <h3 className="text-center">{data.f_name} {data.l_name}</h3>
				        <img className="img-fluid center-block" style={{height:'250px'}} src={data.image_url} alt=""/> 
				    </div>
				 
					<hr/>
				    <section className="information">
				      	<div className="container">
					        <Table bordered>
					          	<thead>
						            <tr>
						              	<th scope="col">Attribute</th>
						              	<th scope="col">Info</th>
						            </tr>
					          	</thead>
					          	<tbody>
					          		<tr>
		                            	<td>Full Name</td>
		                            	<td>{data.f_name} {data.m_name} {data.l_name}</td>
		                            </tr>
		                            <tr>
		                            	<td>Date of Birth</td>
		                            	<td>{data.date_of_birth}</td>
		                            </tr>
		                            <tr>
		                            	<td>State</td>
		                            	<td>{data.state ? data.state : 'N/A'}</td>
		                            </tr>
		                            <tr>
		                            	<td>District</td>
		                            	{this.renderDistrict()}
		                            </tr>
		                            <tr>
		                            	<td>Party</td>
		                            	<td>{data.party ? data.party : 'N/A'}</td>
		                            </tr>
					          	</tbody>
					        </Table>
					        <hr/>
				      	</div>
				    </section>
				</Col>
				
				<Col sm={4}>
				    <section className="donors list">
				      	<div className="container">
					        <Table bordered>
					          	<thead>
						            <tr>
						              	<th scope="col">Donors</th>
						              	<th scope="col">Amount</th>
						            </tr>
					          	</thead>
					          	<tbody>
					          		{data.donorOneName && <tr>
							                            	{this.renderDonor(data.donorOneName, 'donor1Link')}
							                            	<td>${numberWithCommas(data.donorOneAmount)}</td>
							                            </tr>}
		                            {data.donorTwoName && <tr>
							                            	{this.renderDonor(data.donorTwoName, 'donor2Link')}
							                            	<td>${numberWithCommas(data.donorTwoAmount)}</td>
							                            </tr>}
							        {data.donorThreeName && <tr>
							                            	{this.renderDonor(data.donorThreeName, 'donor3Link')}
							                            	<td>${numberWithCommas(data.donorThreeAmount)}</td>
							                            </tr>}
							        {data.donorFourName && <tr>
							                            	{this.renderDonor(data.donorFourName, 'donor4Link')}
							                            	<td>${numberWithCommas(data.donorFourAmount)}</td>
							                            </tr>}
							        {data.donorFiveName && <tr>
							                            	{this.renderDonor(data.donorFiveName, 'donor5Link')}
							                            	<td>${numberWithCommas(data.donorFiveAmount)}</td>
							                            </tr>}
					          	</tbody>
					        </Table>
				      	</div>
				    </section>
				    <SocialIcon className="tw" style={{margin: 25}} url={"http://twitter.com/" + data.twitter}/>
				    <SocialIcon className="fb" style={{margin: 25}} url={"http://facebook.com/" + data.facebook}/>
				</Col>
				<Col>
				<div className="centerContent">
			    	<div className="selfCenter standardWidth">
			    		<TwitterTimelineEmbed
			    			sourceType="profile"
			    			screenName={data.twitter ? data.twitter : ""}
			    			options={{height: 600, width: 400, valign: 'top'}}	
			    		/>
			    	</div>
			    </div>

				</Col>
			</Row>
		</div>
    );
  }
}

const TO_NAME = 1;
const TO_ABBREVIATED = 2;

function convertRegion(input, to) {
    var states = [
        ['Alabama', 'AL'],
        ['Alaska', 'AK'],
        ['American Samoa', 'AS'],
        ['Arizona', 'AZ'],
        ['Arkansas', 'AR'],
        ['Armed Forces Americas', 'AA'],
        ['Armed Forces Europe', 'AE'],
        ['Armed Forces Pacific', 'AP'],
        ['California', 'CA'],
        ['Colorado', 'CO'],
        ['Connecticut', 'CT'],
        ['Delaware', 'DE'],
        ['District Of Columbia', 'DC'],
        ['Florida', 'FL'],
        ['Georgia', 'GA'],
        ['Guam', 'GU'],
        ['Hawaii', 'HI'],
        ['Idaho', 'ID'],
        ['Illinois', 'IL'],
        ['Indiana', 'IN'],
        ['Iowa', 'IA'],
        ['Kansas', 'KS'],
        ['Kentucky', 'KY'],
        ['Louisiana', 'LA'],
        ['Maine', 'ME'],
        ['Marshall Islands', 'MH'],
        ['Maryland', 'MD'],
        ['Massachusetts', 'MA'],
        ['Michigan', 'MI'],
        ['Minnesota', 'MN'],
        ['Mississippi', 'MS'],
        ['Missouri', 'MO'],
        ['Montana', 'MT'],
        ['Nebraska', 'NE'],
        ['Nevada', 'NV'],
        ['New Hampshire', 'NH'],
        ['New Jersey', 'NJ'],
        ['New Mexico', 'NM'],
        ['New York', 'NY'],
        ['North Carolina', 'NC'],
        ['North Dakota', 'ND'],
        ['Northern Mariana Islands', 'NP'],
        ['Ohio', 'OH'],
        ['Oklahoma', 'OK'],
        ['Oregon', 'OR'],
        ['Pennsylvania', 'PA'],
        ['Puerto Rico', 'PR'],
        ['Rhode Island', 'RI'],
        ['South Carolina', 'SC'],
        ['South Dakota', 'SD'],
        ['Tennessee', 'TN'],
        ['Texas', 'TX'],
        ['US Virgin Islands', 'VI'],
        ['Utah', 'UT'],
        ['Vermont', 'VT'],
        ['Virginia', 'VA'],
        ['Washington', 'WA'],
        ['West Virginia', 'WV'],
        ['Wisconsin', 'WI'],
        ['Wyoming', 'WY'],
    ];
    
    var provinces = [
        ['Alberta', 'AB'],
        ['British Columbia', 'BC'],
        ['Manitoba', 'MB'],
        ['New Brunswick', 'NB'],
        ['Newfoundland', 'NF'],
        ['Northwest Territory', 'NT'],
        ['Nova Scotia', 'NS'],
        ['Nunavut', 'NU'],
        ['Ontario', 'ON'],
        ['Prince Edward Island', 'PE'],
        ['Quebec', 'QC'],
        ['Saskatchewan', 'SK'],
        ['Yukon', 'YT'],
    ];

    var regions = states.concat(provinces);

    if (to == TO_ABBREVIATED) {
        input = input.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); });
        for (let region of regions) {
            if (region[0] == input) {
                return (region[1]);
            }
        }
    } else if (to == TO_NAME) {
        input = input.toUpperCase();
        for (let region of regions) {
            if (region[1] == input) {
                return (region[0]);
            }
        }
    }
}
export default PoliticianInstance;