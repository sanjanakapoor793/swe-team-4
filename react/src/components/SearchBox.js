import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { Button } from 'react-bootstrap';

class SearchBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: null
        }
    }

    handleInput = (e) => {
        this.setState({ search: e.target.value });
    }

    handleEnter = (e) => {
        if (e.key === 'Enter' && this.state.search != null) {
            this.props.history.push(`/search/${this.state.search}?type=${this.props.type}`);
        }
    }

    render() {
        return (
            <div>
                <input type="text" placeholder="Search" onChange={this.handleInput} onKeyPress={this.handleEnter} />
                <Link to={this.state.search == null ? "#" : "/search/" + this.state.search + "?type=" + this.props.type}>
                    <Button variant='light' style={{ margin: '10px' }}>Search</Button>
                </Link>
            </div>
        );
    }
}

export default withRouter(SearchBox);