import React, { Component } from 'react';
import { Container, Col, Row} from 'react-bootstrap';
import SearchBox from './SearchBox';

const BackgroundImage = () => {
  return (
      <div className="bg"></div>
  );
}

/* Main component for the home page */
class Home extends Component {

    render() {
        return (
            <div id='home'>
                <section className="main-banner BackgroundImage">
                    <Container>
                        <div className="caption">
                            <h2 style={{fontSize:'3vw'}}>Search for Congressman, Donor, or District</h2>
                            <SearchBox className="searchBox" type={'all'} />
                        </div>
                    </Container>
                </section>
                <section className="features" style={{background:'#FFFFFF'}}>
                    <Container>
                        <Row>
                            <Col sm>
                                <div className="features-content">
                                    <span className="box1"><span aria-hidden="true" className="icon-dial"></span></span>
                                    <h3>Search by Anything!</h3>
                                    <p>Results return anything on the site related to what you search.</p>
                                </div>
                            </Col>

                            <Col sm>
                                <div className="features-content">
                                    <span className="box1"><span aria-hidden="true" className="icon-search"></span></span>
                                    <h3>Social Media</h3>
                                    <p>Politician and Donor pages have links to their respective social media pages if they exist.</p>
                                </div>
                            </Col>

                            <Col sm>
                                <div className="features-content">
                                    <span className="box1"><span aria-hidden="true" className="icon-search"></span></span>
                                    <h3>Discover</h3>
                                    <p>Immediately see what kind of donors each politician has, and what groups comanies donate to.</p>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
                <section className="counter">
                    <Container>
                        <Row>
                            <Col sm>
                                <div className="counter-text">
                                    <span aria-hidden="true" className="icon-briefcase"></span>
                                    <h3>213</h3>
                                    <p>Total Politicians</p>
                                </div>
                            </Col>

                            <Col sm>
                                <div className="counter-text">
                                    <span className="box1"><span aria-hidden="true" className="icon-expand"></span></span>
                                    <h3>29</h3>
                                    <p>Total Donors</p>
                                </div>
                            </Col>

                            <Col sm>
                                <div className="counter-text">
                                    <span className="box1"><span aria-hidden="true" className="icon-profile-male"></span></span>
                                    <h3>28</h3>
                                    <p>Total Districts</p>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </section>
            </div>
        );
    }
}

export default Home;
