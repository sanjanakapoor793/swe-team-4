import React, { Component } from 'react';
import { API_URL } from './Model';
import Fuse from 'fuse.js';
import { Row, Col, Container, Button, Pagination } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import highlight from '../highlight.js';
import { createUltimatePagination, ITEM_TYPES } from "react-ultimate-pagination";

const PER_PAGE = 10;

const PaginatedPage = createUltimatePagination({
  WrapperComponent: Pagination,
  itemTypeToComponent: {
    [ITEM_TYPES.PAGE]: ({ value, isActive }) => (
      <Pagination.Item data-value={value} active={isActive}>{value}</Pagination.Item>
    ),
    [ITEM_TYPES.ELLIPSIS]: ({ value, isActive, onClick }) => (
      <Pagination.Ellipsis data-value={value} onClick={onClick} />
    ),
    [ITEM_TYPES.FIRST_PAGE_LINK]: ({ isActive, onClick }) => (
      <Pagination.First data-value={1} disabled={isActive} onClick={onClick} />
    ),
    [ITEM_TYPES.PREVIOUS_PAGE_LINK]: ({ value, isActive, onClick }) => (
      <Pagination.Prev data-value={value} disabled={isActive} onClick={onClick} />
    ),
    [ITEM_TYPES.NEXT_PAGE_LINK]: ({ value, isActive, onClick }) => (
      <Pagination.Next data-value={value} disabled={isActive} onClick={onClick} />
    ),
    [ITEM_TYPES.LAST_PAGE_LINK]: ({ value, isActive, onClick }) => (
      <Pagination.Last data-value={value} disabled={isActive} onClick={onClick} />
    ),
  },
});

class Result extends Component {
  render() {
    return (
      <Row>
        <Link to={this.props.url} style={{ marginBottom: "20px" }}>
          <div>
            <h5 style={{ lineHeight: "0px", fontWeight: "normal", textDecoration: "underline" }} dangerouslySetInnerHTML={{ __html: this.props.title }}></h5>
            <p dangerouslySetInnerHTML={{ __html: this.props.text }}></p>
          </div>
        </Link>
      </Row>
    );
  }
}

class SearchResults extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: '',
      tab: 'politicians',
      page: 1,
      politicians: [],
      donors: [],
      districts: [],
      loadedpoliticians: false,
      loadeddistricts: false,
      loadeddonors: false
    }
  }

  componentDidMount() {
    const { match: { params } } = this.props;
    const QUERY = params.search;


    let searchParams = new URLSearchParams(this.props.location.search);
    const type = searchParams.has("type") ? searchParams.get("type") : "all";

    this.setState({type: type});

    if (type !== 'all')
      this.setState({tab: type});

    const POLITICIAN_URL = `${API_URL}/politicians`;
    const DONOR_URL = `${API_URL}/donors`;
    const DISTRICT_URL = `${API_URL}/districts`;

    let politicians = [];
    let donors = [];
    let districts = [];

    let politicianOptions = {
      keys: ['display', 'party', 'state', 'district', 'gender', 'date_of_birth', 'donorOneName', 'donorTwoName', 'donorThreeName', 'donorFourName', 'donorFiveName'],
      threshold: 0.4,
      includeMatches: true,
      tokenize: true
    };

    let donorOptions = {
      keys: ['display', 'state', 'industry', 'website', 'phone_num', 'city'],
      threshold: 0.4,
      includeMatches: true,
      tokenize: true,
      minMatchCharLength: 2
    };

    let districtOptions = {
      keys: ['display', 'majorcity', 'population', 'reps', 'median_income'],
      threshold: 0.4,
      includeMatches: true,
      tokenize: true,
      minMatchCharLength: 2
    };
    fetch(POLITICIAN_URL)
      .then(response => response.json())
      .then(data => {
        data.map(e => {
          e.display = e.f_name + " " + (e.m_name != null ? e.m_name + " " : "") + e.l_name;
          politicians.push(e);
        });
        let fuse = new Fuse(politicians, politicianOptions);
        let result = fuse.search(QUERY);
        result = highlight(QUERY, result);
        this.setState({ politicians: result, loadedpoliticians: true });
      });

    fetch(DONOR_URL)
      .then(response => response.json())
      .then(data => {
        data.map(e => { e.display = e.name; donors.push(e) })
        let fuse = new Fuse(donors, donorOptions);
        let result = fuse.search(QUERY);
        result = highlight(QUERY, result);
        this.setState({ donors: result, loadeddonors: true });
      });

    fetch(DISTRICT_URL)
      .then(response => response.json())
      .then(data => {
        data.map(e => {
          e.display = e.name;
          districts.push(e);
        });
        let fuse = new Fuse(districts, districtOptions);
        let result = fuse.search(QUERY);
        result = highlight(QUERY, result);
        this.setState({ districts: result, loadeddistricts: true });
      });
  }

  renderPoliticianResults() {
    return this.state.tab !== 'politicians' ? <p></p> : !this.state.loadedpoliticians ? <p>Loading..</p> : this.state.politicians.length === 0 ? <p>No politicians found</p> : (
      <div>
        {this.state.politicians.slice((this.state.page - 1) * PER_PAGE, ((this.state.page - 1) * PER_PAGE) + PER_PAGE).map(e => {
          return (
            <Result key={e.db_id} url={"/politician?first=" + e.f_name + "&last=" + e.l_name} title={e.display} text={`Party: ${e.party} • State: ${e.state} • District: ${e.district} • Gender: ${e.gender} • Date of Birth: ${e.date_of_birth}`}></Result>
          );
        })}
      </div>
    );
  }

  renderDonorResults() {
    return this.state.tab !== 'donors' ? <p></p> : !this.state.loadeddonors ? <p>Loading..</p> : this.state.donors.length === 0 ? <p>No donors found</p> : (
      <div>
        {this.state.donors.slice((this.state.page - 1) * PER_PAGE, ((this.state.page - 1) * PER_PAGE) + PER_PAGE).map(e => {
          return (
            <Result key={e.db_id} url={"/donor/" + e.name} title={e.display} text={`Location: ${e.state} • Industry: ${e.industry} • Phone #: ${e.phone_num} • Website: ${e.website}`}></Result>
          );
        })}
      </div>
    );
  }

  renderDistrictResults() {
    return this.state.tab !== 'districts' ? <p></p> : !this.state.loadeddistricts ? <p>Loading..</p> : this.state.districts.length === 0 ? <p>No districts found</p> : (
      <div>
        {this.state.districts.slice((this.state.page - 1) * PER_PAGE, ((this.state.page - 1) * PER_PAGE) + PER_PAGE).map(e => {
          return (
            <Result key={e.db_id} url={"/district/" + e.name} title={e.majorcity} text={`District: ${e.display} • Party: ${e.rep_party} • Representative: ${e.reps} • Population: ${e.population} • Median Income: $${e.median_income}`}></Result>
          );
        })}
      </div>
    );
  }

  renderModelTabs(tab) {
    if (this.state.type === 'all') {
      return (
        <Row style={{ justifyContent: "center", marginBottom: "40px" }}>
          <Col className={"text-center"}>
            <Button onClick={e => { this.setState({ tab: "politicians", page: 1 }) }} variant={tab === 'politicians' ? 'dark' : 'outline-dark'}>Politicians</Button>
          </Col>
          <Col className={"text-center"}>
            <Button onClick={e => { this.setState({ tab: "donors", page: 1 }) }} variant={tab === 'donors' ? 'dark' : 'outline-dark'}>Donors</Button>
          </Col>
          <Col className={"text-center"}>
            <Button onClick={e => { this.setState({ tab: "districts", page: 1 }) }} variant={tab === 'districts' ? 'dark' : 'outline-dark'}>Districts</Button>
          </Col>
        </Row>
      );
    }
    else {
      return (<div></div>);
    }
  }

  onClick = (event) => {
    const a = event.target;
    const pageNumber = a.dataset.value ? parseInt(a.dataset.value, 10) : parseInt(a.parentNode.dataset.value, 10);
    if(pageNumber <= this.getTotalPages())
      this.setState({ page: pageNumber });
  }

  renderPagination() {
    let total = this.getTotalPages();
    if (total > 0) {
      return (
        <PaginatedPage
          totalPages={total}
          currentPage={this.state.page}
          onClick={this.onClick}
        />
      );
    }
    else {
      return (<div></div>);
    }
  }

  getTotalPages() {
    switch (this.state.tab) {
      case 'politicians':
        return Math.ceil(this.state.politicians.length / PER_PAGE);
      case 'donors':
        return Math.ceil(this.state.donors.length / PER_PAGE);
      case 'districts':
        return Math.ceil(this.state.districts.length / PER_PAGE);
    }
  }

  render() {
    return (
      <Container style={{ textAlign: "left" }}>
        <h1 style={{ textAlign: "center" }}>Search results for {this.props.match.params.search}</h1>
        <div>{this.renderModelTabs(this.state.tab)}</div>
        <div>{this.renderPoliticianResults()}</div>
        <div>{this.renderDonorResults()}</div>
        <div>{this.renderDistrictResults()}</div>
        <div>{this.renderPagination()}</div>
      </Container>
    );
  }
}

export default SearchResults;