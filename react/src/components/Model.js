import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Card, Table, Pagination, Row, Dropdown, DropdownButton, Col, ButtonToolbar, ButtonGroup } from 'react-bootstrap';
import SearchBox from './SearchBox';
import { createUltimatePagination, ITEM_TYPES } from "react-ultimate-pagination";

const API_URL = 'https://cors.io/?https://api.politicianstakingmoney.me';

const flexStyle = {
	display: 'flex',
	flexWrap: 'wrap',
	justifyContent: 'center'
};

const getAge = (dob) => {
	let year = parseInt(dob.substring(0, 4));
	return 2019 - year;
}

const PaginatedPage = createUltimatePagination({
	WrapperComponent: Pagination,
	itemTypeToComponent: {
		[ITEM_TYPES.PAGE]: ({ value, isActive }) => (
			<Pagination.Item data-value={value} active={isActive}>{value}</Pagination.Item>
		),
		[ITEM_TYPES.ELLIPSIS]: ({ value, isActive, onClick }) => (
			<Pagination.Ellipsis data-value={value} onClick={onClick} />
		),
		[ITEM_TYPES.FIRST_PAGE_LINK]: ({ isActive, onClick }) => (
			<Pagination.First data-value={1} disabled={isActive} onClick={onClick} />
		),
		[ITEM_TYPES.PREVIOUS_PAGE_LINK]: ({ value, isActive, onClick }) => (
			<Pagination.Prev data-value={value} disabled={isActive} onClick={onClick} />
		),
		[ITEM_TYPES.NEXT_PAGE_LINK]: ({ value, isActive, onClick }) => (
			<Pagination.Next data-value={value} disabled={isActive} onClick={onClick} />
		),
		[ITEM_TYPES.LAST_PAGE_LINK]: ({ value, isActive, onClick }) => (
			<Pagination.Last data-value={value} disabled={isActive} onClick={onClick} />
		),
	},
});

const politicianSortBy = [
	{
		display: 'First Name ↑',
		column: 'f_name',
		order: 'asc'
	},
	{
		display: 'First Name ↓',
		column: 'f_name',
		order: 'desc'
	},
	{
		display: 'Last Name ↑',
		column: 'l_name',
		order: 'asc'
	},
	{
		display: 'Last Name ↓',
		column: 'l_name',
		order: 'desc'
	},
	{
		display: 'District ↑',
		column: 'district',
		order: 'asc'
	},
	{
		display: 'District ↓',
		column: 'district',
		order: 'desc'
	}
];

const politicianFilterBy = [
	{
		display: 'State',
		column: 'state',
		options: ['AL', 'AK', 'AS', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'FM', 'FL', 'GA', 'GU', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS',
			'KY', 'LA', 'ME', 'MH', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'MP', 'OH', 'OK',
			'OR', 'PW', 'PA', 'PR', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VI', 'VA', 'WA', 'WV', 'WI', 'WY']
	},
	{
		display: 'Party',
		column: 'party',
		options: ['R', 'D']
	},
	{
		display: 'Gender',
		column: 'gender',
		options: ['M', 'F']
	}
];

const donorSortBy = [
	{
		display: 'Republican Donations ↑',
		column: 'rep_party',
		order: 'asc'
	},
	{
		display: 'Republican Donations ↓',
		column: 'rep_party',
		order: 'desc'
	},
	{
		display: 'Democrat Donations ↑',
		column: 'dem_party',
		order: 'asc'
	},
	{
		display: 'Democrat Donations ↓',
		column: 'dem_party',
		order: 'desc'
	}
];

const donorFilterBy = [
	{
		display: 'State',
		column: 'state',
		options: ['Alabama', 'Alaska', 'American Samoa', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'District of Columbia',
			'Federated States of Micronesia', 'Florida', 'Georgia', 'Guam', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine',
			'Marshall Islands', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey',
			'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Northern Mariana Islands', 'Ohio', 'Oklahoma', 'Oregon', 'Palau', 'Pennsylvania', 'Puerto Rico',
			'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virgin Island', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming']
	},
	{
		display: 'Min Democrat Donations $',
		column: 'dem_party',
		options: ['10000', '50000', '100000', '500000']
	},
	{
		display: 'Min Republican Donations $',
		column: 'rep_party',
		options: ['10000', '50000', '100000', '500000']
	}
];

const districtSortBy = [
	{
		display: 'Median Income ↑',
		column: 'median_income',
		order: 'asc'
	},
	{
		display: 'Median Income ↓',
		column: 'median_income',
		order: 'desc'
	},
	{
		display: 'Population ↑',
		column: 'population',
		order: 'asc'
	},
	{
		display: 'Population ↓',
		column: 'population',
		order: 'desc'
	}
];

const districtFilterBy = [
	{
		display: 'Party',
		column: 'rep_party',
		options: ['R', 'D']
	},
	{
		display: 'Major City',
		column: 'major_city',
		options: ['Mobile', 'Montgomery', 'Decatur', 'Jackson', 'Birmingham', 'Selma', 'Navajo Nation', 'Tucson', 'Phoenix', 'Gilbert', 'Jonesboro', 'Little rock',
			'Springdale', 'Camden', 'San Rafael', 'Sacramento', 'Denver', 'Boulder', 'Grand Junction', 'Greeley', 'Colorado Springs']
	},
	{
		display: 'Min Median Income $',
		column: 'median_income',
		options: ['50000', '70000']
	}
];


class Model extends Component {
	constructor(props) {
		super(props);

		this.state = {
			type: props.type.toLowerCase(),
			page: 1,
			per_page: 9,
			total_pages: 10,
			data: null,
			loaded: false,
			sortBy: null,
			filterBy: [] //this array will contain objects of the format {column: column, option: option} which denotes a currently active filter on column with option selected
						 // ex. {column: party, option: 'D'} denotes filtering the politician results for democrats
		};
	}
	refreshState() {
		if(this.state.loaded)
			return;
		let model = this.state.type + "s";
		let page = this.state.page;
		let per_page = this.state.per_page;
		
		let url = '';
		if(this.state.sortBy === null && this.state.filterBy.length === 0)
			url = `${API_URL}/${model}?page=${page - 1}&per_page=${per_page}`;
		else{
			let filterString = '';
			let sortString = '';

			if(this.state.sortBy !== null){
				sortString = `&sort=${this.state.sortBy.column}&asc=${this.state.sortBy.order === 'asc' ? 1 : 0}`
			}

			if(this.state.filterBy.length > 0){
				filterString = '&fil=';
				this.state.filterBy.forEach(filter => {
					filterString += `${filter.column}!${filter.option} `;
				});
			}
			url = `${API_URL}/${model}/sortfil?page=${page - 1}&per_page=${per_page}${sortString}${filterString}`;
		}

		let myRequest = new Request(url);

		fetch(myRequest)
			.then(response => response.json())
			.then(data => this.setState({ data: data, total_pages: data[data.length - 1].total_pgs, loaded: true }));
	}

	componentDidMount() {
		this.refreshState();
	}

	componentDidUpdate(prevProps, prevState) {
		if (this.state.page !== prevState.page || this.state.sortBy !== prevState.sortBy || !this.state.loaded) {
			this.refreshState();
		}
	}

	createDonorCard(data) {
		return (
			<Link className="donorCard" key={data.db_id} to={'/donor/' + data.name}>
				<Card style={{ width: '600px', margin: '10px' }}>
					<Card.Img className="donorimg" variant="top" src={data.logourl} alt="" />
					<Card.Body>
						<Card.Title>{data.name}</Card.Title>
						<Card.Subtitle className="mb-2 text-muted">{data.industry}</Card.Subtitle>
						<Table>
							<thead>
								<tr>
									<th scope="col">State</th>
									<th scope="col">City</th>
									<th scope="col">Democrat Donations</th>
									<th scope="col">Republican Donations</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>{data.state}</td>
									<td>{data.city}</td>
									<td>${data.demparty}</td>
									<td>${data.repparty}</td>
								</tr>
							</tbody>
						</Table>
					</Card.Body>
				</Card>
			</Link>
		);
	}
	createDistrictCard(data) {
		return (
			<Link className="districtCard" key={data.db_id} to={'/district/' + data.name}>
				<Card style={{ width: '350px', margin: '10px' }}>
					<Card.Img className="districtimg" variant="top" src={data.imageurl} alt="" />
					<Card.Body>
						<Card.Title>{data.majorcity}</Card.Title>
						<Card.Subtitle className="mb-2 text-muted">{data.name}</Card.Subtitle>
						<Table>
							<thead>
								<tr>
									<th scope="col">Party</th>
									<th scope="col">Median Income</th>
									<th scope="col">Population</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>{data.rep_party}</td>
									<td>${data.median_income}</td>
									<td>{data.population}</td>
								</tr>
							</tbody>
						</Table>
					</Card.Body>
				</Card>
			</Link>
		);
	}
	createPoliticianCard(data) {
		return (
			<Link className="politicianCard" key={data.crp_id} to={{ pathname: '/politician', search: '?first=' + data.f_name + "&last=" + data.l_name }}>
				<Card style={{ width: '350px', margin: '10px' }}>
					<Card.Img className="politicianimg" variant="top" src={data.image_url} alt="" />
					<Card.Body>
						<Card.Title>{data.f_name + " " + data.l_name}</Card.Title>
						<Card.Subtitle className="mb-2 text-muted">{data.state}</Card.Subtitle>
						<Table>
							<thead>
								<tr>
									<th scope="col">District</th>
									<th scope="col">Gender</th>
									<th scope="col">Party</th>
									<th scope="col">Age</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>{data.district}</td>
									<td>{data.gender}</td>
									<td>{data.party}</td>
									<td>{getAge(data.date_of_birth)}</td>
								</tr>
							</tbody>
						</Table>
					</Card.Body>
				</Card>
			</Link>
		);
	}

	onClick = (event) => {
		const a = event.target;
		const pageNumber = a.dataset.value ? parseInt(a.dataset.value, 10) : parseInt(a.parentNode.dataset.value, 10);
		if (pageNumber <= this.state.total_pages && pageNumber !== this.state.page)
			this.setState({ page: pageNumber, loaded: false });
	}

	renderPagination() {
		if(this.state.total_pages === 0)
			return (<div></div>);
		else
			return (
				<PaginatedPage
					totalPages={this.state.total_pages}
					currentPage={this.state.page}
					onClick={this.onClick}
				/>
			);
	}

	generateSortButton() {
		let sortItems = null;
		switch(this.props.type){
			case 'Politician':
				sortItems = politicianSortBy;
				break;
			case 'Donor':
				sortItems = donorSortBy;
				break;
			case 'District':
				sortItems = districtSortBy;
				break;
			default:
				console.log('Error generating search button');
				break;
		};

		//Higher order function that returns a unique onClick function for each item
		const onSortClick = (item) => {
			return (e) => {
				let val = item.display;
				//User clicked on the active sort so we disable it
				if(this.state.sortBy && this.state.sortBy.display === val){
					this.setState({sortBy: null, page: 1, loaded: false});
				}
				//Otherwise we enable the sort they clicked on
				else {
					this.setState({sortBy: item, page: 1, loaded: false});
				}
			}
		};

		return (
			<ButtonGroup className="mr-2">
				<DropdownButton id="dropdown-basic-button" title={`Sort By: ${this.state.sortBy ? this.state.sortBy.display : ''}`}>
					{sortItems.map(item => (
					<Dropdown.Item key={item.display} active={this.state.sortBy && this.state.sortBy.display === item.display ? 1 : 0} onClick={onSortClick(item)}>{item.display}</Dropdown.Item>
					))}
				</DropdownButton>
			</ButtonGroup>
		);
	}

	generateFilterButtons() {
		let filterItems = null;
		switch(this.props.type){
			case 'Politician':
				filterItems = politicianFilterBy;
				break;
			case 'Donor':
				filterItems = donorFilterBy;
				break;
			case 'District':
				filterItems = districtFilterBy;
				break;
			default:
				console.log('Error generating filter buttons');
				break;
		}

		//Higher order function that returns a unique onClick function for each item
		const onFilterClick = (item, option) => {
			return (e) => {
				let filtersearch = this.state.filterBy.filter(filteredItem => filteredItem.column === item.column);
				//Check if there is already a filter of the same type active
				if(filtersearch.length > 0){
					//They clicked on the same option, so disable it
					if(filtersearch[0].option === option){
						this.setState({filterBy: this.state.filterBy.filter(filteredItem => filteredItem.column !== item.column), page: 1, loaded: false});
					}
					//They clicked on a different option of the same filter, so switch the filter to the new option
					else {
						filtersearch[0].option = option;
						this.setState({filterBy: this.state.filterBy, page: 1, loaded: false});
					}
				}
				//Otherwise we enable the filter they clicked on
				else {
					let filteredItems = this.state.filterBy;
					filteredItems.push({column: item.column, option: option, display: item.display});
					this.setState({filterBy: filteredItems, page: 1, loaded: false});
				}
			}
		};

		const getOption = (item) => {
			let filterSearch = this.state.filterBy.filter(filter => filter.column === item.column);
			let setting = '';
			if(filterSearch.length > 0)
				setting = filterSearch[0].option;
			return setting;
		}

		return (
			<div>
				{filterItems.map(item => (
					<ButtonGroup key={item.display} className="mr-2">
						<DropdownButton title={`${item.display}: ${getOption(item)}`}>
							{item.options.map(option => (
								<Dropdown.Item 
									onClick={onFilterClick(item, option)} 
									active={this.state.filterBy.some(filteredItem => filteredItem.column === item.column && filteredItem.option === option) ? 1 : 0} 
									key={option}>
								{option}
								</Dropdown.Item>
							))}
						</DropdownButton>
					</ButtonGroup>
				))}
			</div>
		);
	}

	render() {
		return this.state.loaded ? (
			<div>
				<h1>{this.props.type + "s"}</h1>

				<ButtonToolbar style={{ marginLeft: 50 }}>
					{this.generateSortButton()}
					{this.generateFilterButtons()}
					<SearchBox type={this.state.type + 's'}/>
				</ButtonToolbar>

				<div style={flexStyle}>
					{this.state.data != null ? this.state.data.map((entry, index, arr) =>
						index === arr.length - 1 ? <div key={'empty'} /> :
							this.state.type === "politician" ? this.createPoliticianCard(entry) :
								this.state.type === "donor" ? this.createDonorCard(entry) :
									this.state.type === "district" ? this.createDistrictCard(entry) :
										<p key={'not found'}>Not Found</p>
					) : <p>loading</p>}
				</div>
				{this.renderPagination()}
			</div>
		) : <p>Loading</p>;
	}
}

export { API_URL, getAge };
export default Model;